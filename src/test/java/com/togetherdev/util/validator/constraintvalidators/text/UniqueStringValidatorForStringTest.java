package com.togetherdev.util.validator.constraintvalidators.text;

import com.togetherdev.util.validator.constraints.text.UniqueString;
import com.togetherdev.util.validator.constraints.text.UniqueString.UniqueStringTester;
import com.togetherdev.util.validator.util.MockConstraintValidatorContext;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UniqueStringValidatorForStringTest {

    @Test
    public void testIsValid() {
        MockConstraintValidatorContext context = MockConstraintValidatorContext.getInstance();
        UniqueStringValidatorForString instance = new UniqueStringValidatorForString();
        instance.initialize(createAnnotation());
        Tester.values.forEach(v -> {
            boolean valid = instance.isValid(v, context);
            assertFalse("isValid", valid);
            assertFalse("isEmpty", context.isEmpty());
            context.clear();
        });
    }

    private static UniqueString createAnnotation() {
        UniqueString annotation = mock(UniqueString.class);
        when(annotation.message()).thenReturn("UNIQUE_STRING");
        when(annotation.tester()).then(invocation -> Tester.class);
        when(annotation.message()).thenReturn("it is not unique");
        return annotation;
    }

    public static class Tester implements UniqueStringTester {

        private static Set<String> values = new HashSet<>(Arrays.asList("a", "b", "c", "d", "e"));

        @Override
        public boolean isUnique(String value) {
            return !values.contains(value);
        }

    }

}
