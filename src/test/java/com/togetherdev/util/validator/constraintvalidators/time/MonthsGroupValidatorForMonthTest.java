package com.togetherdev.util.validator.constraintvalidators.time;

import com.togetherdev.util.validator.constraints.time.MonthsGroup;
import com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidatorTest;
import java.time.Month;
import java.util.Arrays;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MonthsGroupValidatorForMonthTest extends AbstractGroupValidatorTest<MonthsGroup, Month, Month> {

    private static final Month MIN = Month.FEBRUARY;
    private static final Month MAX = Month.NOVEMBER;

    public MonthsGroupValidatorForMonthTest() {
        super(new MonthsGroupValidatorForMonth(), createAnnotation());
    }

    @Override
    public void setUp() {
        VALUES.clear();
        for (Month month : Month.values()) {
            VALUES.put(month, ((month.compareTo(MIN) >= 0) && (month.compareTo(MAX) <= 0)));
        }
    }

    private static MonthsGroup createAnnotation() {
        MonthsGroup annotation = mock(MonthsGroup.class);
        when(annotation.message()).thenReturn("message message");
        Month[] months = Arrays.copyOfRange(Month.values(), MIN.ordinal(), (MAX.ordinal() + 1));
        when(annotation.months()).thenReturn(months);
        return annotation;
    }

}
