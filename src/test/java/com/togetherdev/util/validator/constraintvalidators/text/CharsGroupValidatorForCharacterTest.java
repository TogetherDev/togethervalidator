package com.togetherdev.util.validator.constraintvalidators.text;

import com.togetherdev.util.validator.constraints.text.CharsGroup;
import com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidatorTest;
import static com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidatorTest.negateValues;
import static com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidatorTest.testEachValue;
import static com.togetherdev.util.validator.util.LoggerManager.LOGGER;
import static java.lang.Character.toLowerCase;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CharsGroupValidatorForCharacterTest extends AbstractGroupValidatorTest<CharsGroup, Character, Character> {

    private static final int MIN = 'A';
    private static final int MAX = 'Z';

    public CharsGroupValidatorForCharacterTest() {
        super(new CharsGroupValidatorForCharacter(), createAnnotation());
    }

    @Override
    public void setUp() {
        when(ANNOTATION.negated()).thenReturn(false);
        when(ANNOTATION.caseSensitive()).thenReturn(true);
        when(ANNOTATION.message()).thenReturn("group message");
        initializeValues(true);
    }

    public void initializeValues(boolean caseSensitive) {
        VALUES.clear();
        for (int i = (MIN - 10); i <= (MAX + 10); i++) {
            char c = (char) i;
            VALUES.put(c, ((i >= MIN) && (i <= MAX)));
        }
        for (int i = MIN; i <= MAX; i++) {
            char c = (char) i;
            VALUES.put(toLowerCase(c), (((i >= MIN) && (i <= MAX)) && !caseSensitive));
        }
    }

    @Test
    public void insensitiveGroupTest() {
        LOGGER.info("[INSENSITIVE CASE] Testing values in and out of the group...");
        when(ANNOTATION.caseSensitive()).thenReturn(false);
        VALIDATOR.initialize(ANNOTATION);
        initializeValues(false);
        testEachValue(VALUES, VALIDATOR);
    }

    @Test
    public void negatedInsensitiveGroupTest() {
        LOGGER.info("[NEGATED] [INSENSITIVE CASE] Testing values in and out of the group...");
        when(ANNOTATION.caseSensitive()).thenReturn(false);
        when(ANNOTATION.negated()).thenReturn(true);
        VALIDATOR.initialize(ANNOTATION);
        initializeValues(false);
        negateValues(VALUES);
        testEachValue(VALUES, VALIDATOR);
    }

    private static CharsGroup createAnnotation() {
        CharsGroup annotation = mock(CharsGroup.class);
        char[] values = new char[(MAX - MIN) + 1];
        for (int i = MIN; i <= MAX; i++) {
            values[i - MIN] = (char) i;
        }
        when(annotation.values()).thenReturn(values);
        return annotation;
    }

}
