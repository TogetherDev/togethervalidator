package com.togetherdev.util.validator.constraintvalidators.number;

import com.togetherdev.util.validator.constraints.number.DecimalsRange;
import com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidatorTest;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DecimalsGroupValidatorForNumberTest extends AbstractRangeValidatorTest<DecimalsRange, Number> {

    private static final int MIN = 10;
    private static final int MAX = 20;

    public DecimalsGroupValidatorForNumberTest() {
        super(new DecimalsRangeValidatorForNumber(), createAnnotation());
    }

    @Override
    public void setUp() {
        VALUES.clear();
        for (double i = (MIN - 10); i < (MAX + 10); i++) {
            VALUES.put(i, ((i >= MIN) && (i <= MAX)));
        }
    }

    private static DecimalsRange createAnnotation() {
        DecimalsRange boundsAnnotation = mock(DecimalsRange.class);
        when(boundsAnnotation.min()).thenReturn((double) MIN);
        when(boundsAnnotation.max()).thenReturn((double) MAX);
        when(boundsAnnotation.message()).thenReturn("Range message");
        return boundsAnnotation;
    }

}
