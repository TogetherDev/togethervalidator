package com.togetherdev.util.validator.constraintvalidators.time;

import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForInstant;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForLocalDate;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForLocalDateTime;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForLocalTime;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForOffsetDateTime;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForOffsetTime;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForYear;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForYearMonth;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForZonedDateTime;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZonedDateTime;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.time.temporal.ChronoUnit.MONTHS;
import static java.time.temporal.ChronoUnit.NANOS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.time.temporal.ChronoUnit.WEEKS;
import static java.time.temporal.ChronoUnit.YEARS;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

@RunWith(Enclosed.class)
public class FutureValidatorsTest {

    public static class FutureValidatorForInstantTest extends AbstractTemporalValidatorTest<Instant> {

        public FutureValidatorForInstantTest() {
            super(true, new FutureValidatorForInstant(), zoneId -> Instant.now(Clock.system(zoneId)), MILLIS);
        }

    }

    public static class FutureValidatorForLocalDateTest extends AbstractTemporalValidatorTest<LocalDate> {

        public FutureValidatorForLocalDateTest() {
            super(true, new FutureValidatorForLocalDate(), LocalDate::now, DAYS);
        }

    }

    public static class FutureValidatorForLocalDateTimeTest extends AbstractTemporalValidatorTest<LocalDateTime> {

        public FutureValidatorForLocalDateTimeTest() {
            super(true, new FutureValidatorForLocalDateTime(), LocalDateTime::now, MINUTES);
        }

    }

    public static class FutureValidatorForLocalTimeTest extends AbstractTemporalValidatorTest<LocalTime> {

        public FutureValidatorForLocalTimeTest() {
            super(true, new FutureValidatorForLocalTime(), LocalTime::now, SECONDS);
        }

    }

    public static class FutureValidatorForOffsetDateTimeTest extends AbstractTemporalValidatorTest<OffsetDateTime> {

        public FutureValidatorForOffsetDateTimeTest() {
            super(true, new FutureValidatorForOffsetDateTime(), OffsetDateTime::now, HOURS);
        }

    }

    public static class FutureValidatorForOffsetTimeTest extends AbstractTemporalValidatorTest<OffsetTime> {

        public FutureValidatorForOffsetTimeTest() {
            super(true, new FutureValidatorForOffsetTime(), OffsetTime::now, NANOS);
        }

    }

    public static class FutureValidatorForZonedDateTimeTest extends AbstractTemporalValidatorTest<ZonedDateTime> {

        public FutureValidatorForZonedDateTimeTest() {
            super(true, new FutureValidatorForZonedDateTime(), ZonedDateTime::now, WEEKS);
        }

    }

    public static class FutureValidatorForYearTest extends AbstractTemporalValidatorTest<Year> {

        public FutureValidatorForYearTest() {
            super(true, new FutureValidatorForYear(), Year::now, YEARS);
        }

    }

    public static class FutureValidatorForYearMonthTest extends AbstractTemporalValidatorTest<YearMonth> {

        public FutureValidatorForYearMonthTest() {
            super(true, new FutureValidatorForYearMonth(), YearMonth::now, MONTHS);
        }

    }

}
