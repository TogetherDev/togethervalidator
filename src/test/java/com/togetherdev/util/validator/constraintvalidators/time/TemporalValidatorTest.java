package com.togetherdev.util.validator.constraintvalidators.time;

import com.togetherdev.util.validator.constraintvalidators.time.PastValidators.PastValidatorForInstant;
import com.togetherdev.util.validator.constraintvalidators.time.PastValidators.PastValidatorForLocalDate;
import com.togetherdev.util.validator.constraintvalidators.time.PastValidators.PastValidatorForLocalDateTime;
import com.togetherdev.util.validator.constraintvalidators.time.PastValidators.PastValidatorForLocalTime;
import com.togetherdev.util.validator.constraintvalidators.time.PastValidators.PastValidatorForOffsetDateTime;
import com.togetherdev.util.validator.constraintvalidators.time.PastValidators.PastValidatorForOffsetTime;
import com.togetherdev.util.validator.constraintvalidators.time.PastValidators.PastValidatorForYear;
import com.togetherdev.util.validator.constraintvalidators.time.PastValidators.PastValidatorForYearMonth;
import com.togetherdev.util.validator.constraintvalidators.time.PastValidators.PastValidatorForZonedDateTime;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZonedDateTime;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.MILLIS;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.time.temporal.ChronoUnit.MONTHS;
import static java.time.temporal.ChronoUnit.NANOS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.time.temporal.ChronoUnit.WEEKS;
import static java.time.temporal.ChronoUnit.YEARS;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

@RunWith(Enclosed.class)
public class TemporalValidatorTest {

    public static class PastValidatorForInstantTest extends AbstractTemporalValidatorTest<Instant> {

        public PastValidatorForInstantTest() {
            super(false, new PastValidatorForInstant(), zoneId -> Instant.now(Clock.system(zoneId)), MILLIS);
        }

    }

    public static class PastValidatorForLocalDateTest extends AbstractTemporalValidatorTest<LocalDate> {

        public PastValidatorForLocalDateTest() {
            super(false, new PastValidatorForLocalDate(), LocalDate::now, DAYS);
        }

    }

    public static class PastValidatorForLocalDateTimeTest extends AbstractTemporalValidatorTest<LocalDateTime> {

        public PastValidatorForLocalDateTimeTest() {
            super(false, new PastValidatorForLocalDateTime(), LocalDateTime::now, MINUTES);
        }

    }

    public static class PastValidatorForLocalTimeTest extends AbstractTemporalValidatorTest<LocalTime> {

        public PastValidatorForLocalTimeTest() {
            super(false, new PastValidatorForLocalTime(), LocalTime::now, SECONDS);
        }

    }

    public static class PastValidatorForOffsetDateTimeTest extends AbstractTemporalValidatorTest<OffsetDateTime> {

        public PastValidatorForOffsetDateTimeTest() {
            super(false, new PastValidatorForOffsetDateTime(), OffsetDateTime::now, HOURS);
        }

    }

    public static class PastValidatorForOffsetTimeTest extends AbstractTemporalValidatorTest<OffsetTime> {

        public PastValidatorForOffsetTimeTest() {
            super(false, new PastValidatorForOffsetTime(), OffsetTime::now, NANOS);
        }

    }

    public static class PastValidatorForZonedDateTimeTest extends AbstractTemporalValidatorTest<ZonedDateTime> {

        public PastValidatorForZonedDateTimeTest() {
            super(false, new PastValidatorForZonedDateTime(), ZonedDateTime::now, WEEKS);
        }

    }

    public static class PastValidatorForYearTest extends AbstractTemporalValidatorTest<Year> {

        public PastValidatorForYearTest() {
            super(false, new PastValidatorForYear(), Year::now, YEARS);
        }

    }

    public static class PastValidatorForYearMonthTest extends AbstractTemporalValidatorTest<YearMonth> {

        public PastValidatorForYearMonthTest() {
            super(false, new PastValidatorForYearMonth(), YearMonth::now, MONTHS);
        }

    }

}
