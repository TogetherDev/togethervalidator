package com.togetherdev.util.validator.constraintvalidators.time;

import static com.togetherdev.util.format.time.StandardDateTimeFormatter.TO_STRING;
import com.togetherdev.util.validator.constraints.time.Future;
import com.togetherdev.util.validator.constraints.time.Past;
import com.togetherdev.util.validator.util.InstanceFactory;
import static com.togetherdev.util.validator.util.LoggerManager.LOGGER;
import com.togetherdev.util.validator.util.MockConstraintValidatorContext;
import com.togetherdev.util.validator.util.UserZoneIdProvider;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import static java.util.Objects.requireNonNull;
import java.util.function.Consumer;
import java.util.function.Function;
import static java.util.logging.Level.INFO;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.invocation.InvocationOnMock;

public abstract class AbstractTemporalValidatorTest<T extends Temporal & Comparable<? super T>> {

    private final boolean FUTURE;
    private final TemporalAnnotation TEMPORAL_ANNOTATION;
    private final MockConstraintValidatorContext CONTEXT;
    private final Consumer<TemporalAnnotation> DIFFERECE_INITIALIZER;
    private Function<ZoneId, T> currentTimeSupplier;
    private AbstractTemporalValidator<? super TemporalAnnotation, T> instance;
    private T fakeCurrentTime;

    public AbstractTemporalValidatorTest(boolean future, AbstractTemporalValidator<? super TemporalAnnotation, T> instance, Function<ZoneId, T> currentTimeSupplier, ChronoUnit differenceUnit) {
        this(future, instance, currentTimeSupplier, annotation -> {
            when(annotation.minDifference()).thenReturn(1L);
            when(annotation.maxDifference()).thenReturn(2L);
            when(annotation.minDifferenceUnit()).thenReturn(differenceUnit);
            when(annotation.maxDifferenceUnit()).thenReturn(differenceUnit);
        });
    }

    public AbstractTemporalValidatorTest(boolean future, AbstractTemporalValidator<? super TemporalAnnotation, T> instance, Function<ZoneId, T> currentTimeSupplier, Consumer<TemporalAnnotation> differeceInitializer) {
        this.FUTURE = future;
        this.currentTimeSupplier = requireNonNull(currentTimeSupplier);
        this.instance = requireNonNull(instance);
        this.DIFFERECE_INITIALIZER = requireNonNull(differeceInitializer);
        this.CONTEXT = MockConstraintValidatorContext.getInstance();
        this.TEMPORAL_ANNOTATION = mock(TemporalAnnotation.class);
    }

    @Before
    public void setUp() {
        when(TEMPORAL_ANNOTATION.defaultZoneIdProvider()).then((InvocationOnMock invocation) -> {
            return UserZoneIdProvider.class;
        });
        when(TEMPORAL_ANNOTATION.currentTimeValid()).thenReturn(false);
        when(TEMPORAL_ANNOTATION.pattern()).thenReturn("");
        when(TEMPORAL_ANNOTATION.standardFormatter()).thenReturn(TO_STRING);
        when(TEMPORAL_ANNOTATION.message()).thenReturn("temporal message");
        when(TEMPORAL_ANNOTATION.maxLimitMessage()).thenReturn("max limit message");
        DIFFERECE_INITIALIZER.accept(TEMPORAL_ANNOTATION);
        initialize();
    }

    private void initialize() {
        instance.initialize(TEMPORAL_ANNOTATION);
        instance.setCurrentTimeSupplier(this::getFakeCurrentTime);
    }

    @After
    public void tearDown() {
        CONTEXT.clear();
    }

    @Test
    public void validTimeTest() {
        LOGGER.info("Starting the test...");
        timeTest(true);
    }

    @Test
    public void invalidTimeTest() {
        LOGGER.info("Starting the test...");
        timeTest(false);
    }

    private void timeTest(boolean valid) {
        LOGGER.log(INFO, "Testing a {0} min time...", (valid ? "valid" : "invalid"));
        T minLimit = getMinLimit(valid);
        assertEquals("isValid", valid, instance.isValid(minLimit, CONTEXT));
        assertEquals("isEmpty", valid, CONTEXT.isEmpty());
        CONTEXT.clear();

        LOGGER.info("#1 Testing a time equal to current time...");
        when(TEMPORAL_ANNOTATION.minDifference()).thenReturn(0L);
        when(TEMPORAL_ANNOTATION.maxDifference()).thenReturn(0L);
        when(TEMPORAL_ANNOTATION.currentTimeValid()).thenReturn(true);
        initialize();
        T currentTime1 = getCurrentTime();
        assertTrue("isValid", instance.isValid(currentTime1, CONTEXT));
        assertTrue("isEmpty", CONTEXT.isEmpty());
        CONTEXT.clear();

        LOGGER.info("#2 Testing a time equal to current time...");
        when(TEMPORAL_ANNOTATION.currentTimeValid()).thenReturn(false);
        initialize();

        T currentTime2 = getCurrentTime();
        assertFalse("isValid", instance.isValid(currentTime2, CONTEXT));
        assertFalse("isEmpty", CONTEXT.isEmpty());
    }

    @Test
    public void validMinLimitTest() {
        LOGGER.info("Starting the test...");
        limitTest(true, false);
    }

    @Test
    public void invalidMinLimitTest() {
        LOGGER.info("Starting the test...");
        limitTest(false, false);
    }

    @Test
    public void validMaxLimitTest() {
        LOGGER.info("Starting the test...");
        limitTest(true, true);
    }

    @Test
    public void invalidMaxLimitTest() {
        LOGGER.info("Starting the test...");
        limitTest(false, true);
    }

    private void limitTest(boolean valid, boolean max) {
        LOGGER.log(INFO, "Testing a {0} {1} limit...", new String[]{(valid ? "valid" : "invalid"), (max ? "max" : "min")});
        T currentTime = (max ? getMaxLimit(valid) : getMinLimit(valid));
        assertEquals("isValid", valid, instance.isValid(currentTime, CONTEXT));
        assertEquals("isEmpty", valid, CONTEXT.isEmpty());
    }

    @Test
    public void validTimeBoundsTest() {
        LOGGER.info("Starting the test...");
        timeBoundsTest(true);
    }

    @Test
    public void invalidTimeBoundsTest() {
        LOGGER.info("Starting the test...");
        timeBoundsTest(false);
    }

    private void timeBoundsTest(boolean valid) {
        LOGGER.log(INFO, "Testing a {0} time in the bounds...", (valid ? "valid" : "invalid"));
        T minLimit = getMinLimit(valid);
        assertEquals("isValidMin", valid, instance.isValid(minLimit, CONTEXT));
        assertEquals("isEmpty", valid, CONTEXT.isEmpty());
        CONTEXT.clear();

        LOGGER.info("Testing time between bounds.");
        T timeBetweenBounds = getTimeBetweenBounds();
        assertTrue("isValidTimeBetweenBounds", instance.isValid(timeBetweenBounds, CONTEXT));
        assertTrue("isEmpty", CONTEXT.isEmpty());

        LOGGER.info("Testing max limit.");
        T maxLimit = getMaxLimit(valid);
        assertEquals("isValidMax", valid, instance.isValid(maxLimit, CONTEXT));
        assertEquals("isEmpty", valid, CONTEXT.isEmpty());
    }

    private T getCurrentTime() {
        UserZoneIdProvider provider = InstanceFactory.newZoneIdProvider(TEMPORAL_ANNOTATION.defaultZoneIdProvider());
        this.fakeCurrentTime = currentTimeSupplier.apply(provider.getUserZoneId());
        LOGGER.log(INFO, "currentTime = {0}", fakeCurrentTime);
        return fakeCurrentTime;
    }

    private T getFakeCurrentTime(ZoneId zoneId) {
        LOGGER.log(INFO, "FAKE currentTime = {0}", fakeCurrentTime);
        return fakeCurrentTime;
    }

    private T getMinLimit(boolean valid) {
        return getMinLimit(valid, getCurrentTime());
    }

    private T getMinLimit(boolean valid, T currentTime) {
        long minDifference = TEMPORAL_ANNOTATION.minDifference();
        ChronoUnit unit = TEMPORAL_ANNOTATION.minDifferenceUnit();
        T minLimit;
        if (FUTURE) {
            minLimit = (T) (valid ? currentTime.plus(minDifference, unit) : currentTime.minus(1, unit));
        } else {
            minLimit = (T) (valid ? currentTime.minus(minDifference, unit) : currentTime.plus(1, unit));
        }
        LOGGER.log(INFO, "Min limit = {0}", minLimit);
        return minLimit;
    }

    private T getTimeBetweenBounds() {
        T minLimit = getMinLimit(true);
        ChronoUnit unit = TEMPORAL_ANNOTATION.minDifferenceUnit();
        T timeBetweenBounds = (T) (FUTURE ? minLimit.plus(1, unit) : minLimit.minus(1, unit));
        LOGGER.log(INFO, "Time between bounds = {0}", timeBetweenBounds);
        return timeBetweenBounds;
    }

    private T getMaxLimit(boolean valid) {
        return getMaxLimit(valid, getCurrentTime());
    }

    private T getMaxLimit(boolean valid, T currentTime) {
        long maxDifference = TEMPORAL_ANNOTATION.maxDifference();
        ChronoUnit unit = TEMPORAL_ANNOTATION.maxDifferenceUnit();
        if (!valid) {
            maxDifference++;
        }
        T maxLimit = (T) (FUTURE ? currentTime.plus(maxDifference, unit) : currentTime.minus(maxDifference, unit));
        LOGGER.log(INFO, "Max limit = {0}", maxLimit);
        return maxLimit;
    }

    public void setInstance(AbstractTemporalValidator<? super TemporalAnnotation, T> instance) {
        this.instance = instance;
    }

    public void setCurrentTimeSupplier(Function<ZoneId, T> currentTimeSupplier) {
        this.currentTimeSupplier = currentTimeSupplier;
    }

    public static interface TemporalAnnotation extends Past, Future {

    }

}
