package com.togetherdev.util.validator.constraintvalidators;

import static com.togetherdev.util.validator.util.LoggerManager.LOGGER;
import com.togetherdev.util.validator.util.MockConstraintValidatorContext;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;
import static java.util.Objects.requireNonNull;
import java.util.logging.Level;
import javax.validation.ConstraintValidator;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public abstract class AbstractRangeValidatorTest<A extends Annotation, T> {

    protected final A ANNOTATION;
    protected final Map<T, Boolean> VALUES;
    protected final AbstractRangeValidator<A, T> VALIDATOR;

    public AbstractRangeValidatorTest(AbstractRangeValidator<A, T> validator, A boundsAnnotation) {
        this.ANNOTATION = requireNonNull(boundsAnnotation);
        this.VALIDATOR = requireNonNull(validator);
        this.VALUES = new HashMap<>();
    }

    @Before
    public abstract void setUp();

    @Test
    public void rangeTest() {
        LOGGER.info("Testing values in and out of range...");
        VALIDATOR.initialize(ANNOTATION);
        testEachValue(VALUES, VALIDATOR);
    }

    @Test
    public void negatedRangeTest() {
        LOGGER.info("[NEGATED] Testing values in and out of range...");
        VALIDATOR.initialize(ANNOTATION);
        VALIDATOR.negated = true;
        negateValues(VALUES);
        testEachValue(VALUES, VALIDATOR);
    }

    public static <T> void testEachValue(Map<T, Boolean> map, ConstraintValidator<?, T> validator) {
        MockConstraintValidatorContext context = MockConstraintValidatorContext.getInstance();
        map.forEach((value, valid) -> {
            boolean isValid = validator.isValid(value, context);
            boolean isEmpty = context.isEmpty();
            LOGGER.log(Level.FINEST, String.format("The value [%s] (expected = %b) is valid = %b ", value, valid, isValid));
            assertEquals(value + " isValid", valid, isValid);
            assertEquals("isEmpty", valid, isEmpty);
            context.clear();
        });
    }

    public static <T> void negateValues(Map<T, Boolean> map) {
        LOGGER.log(Level.FINER, "Negating values: {0}", map);
        map.entrySet().forEach(entry -> {
            entry.setValue(!entry.getValue());
        });
        LOGGER.log(Level.FINER, "Negated values: {0}", map);
    }

}
