package com.togetherdev.util.validator.constraintvalidators.password;

import com.togetherdev.util.validator.constraints.password.Password;
import com.togetherdev.util.validator.constraints.password.Password.WeakPasswordTester;
import com.togetherdev.util.validator.util.MockConstraintValidatorContext;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PasswordValidatorForStringTest {

    private Password annotation;
    PasswordValidatorForString validator;
    private MockConstraintValidatorContext context;

    public PasswordValidatorForStringTest() {
        this.annotation = mock(Password.class);
        this.validator = new PasswordValidatorForString();
        this.context = MockConstraintValidatorContext.getInstance();
    }

    @Before
    public void setUp() {
        context.clear();
        resetToDefault(annotation);
        validator.initialize(annotation);
    }

    @Test
    public void emptyStringTest() {
        assertFalse(validator.isValid("", context));
    }

    @Test
    public void minLengthTest() {
        assertFalse(validator.isValid("12345678", context));
    }

    @Test
    public void requiredNumbersTest() {
        when(annotation.digits()).thenReturn(1);
        validator.initialize(annotation);
        assertFalse("Only letters 1", validator.isValid("abcdefgh", context));
        assertTrue("One number 1", validator.isValid("abcd4fgh", context));
        assertTrue("Various numbers 1", validator.isValid("ab1c47d4fg898h", context));
        when(annotation.digits()).thenReturn(3);
        validator.initialize(annotation);
        assertFalse("Only letters 2", validator.isValid("abcdefgh", context));
        assertFalse("One number 2", validator.isValid("abcd4fgh", context));
        assertTrue("Three numbers", validator.isValid("a1bc2d3gh", context));
        assertTrue("Various numbers 2", validator.isValid("ab1c467897489d4fg898h", context));
    }

    @Test
    public void requiredAlphabetTest() {
        when(annotation.alphabet()).thenReturn(1);
        validator.initialize(annotation);
        assertFalse("Only letters 1", validator.isValid("abcdefgh", context));
        assertTrue("One number 1", validator.isValid("abcd4fgh", context));
        assertTrue("Various letters 1", validator.isValid("ab1c47d4fg898h", context));
        assertTrue("Various letters 1", validator.isValid("546954498wdcnsfouicdhviuevrbaio50768659347656576u", context));
        when(annotation.alphabet()).thenReturn(3);
        validator.initialize(annotation);
        assertFalse("Only letters 2", validator.isValid("abcdefgh", context));
        assertFalse("One letter 2", validator.isValid("e347859599479", context));
        assertTrue("Three letters", validator.isValid("4930546c0598032a8450469r", context));
        assertTrue("Various letters 2", validator.isValid("ab1c467897489d4fg898h", context));
    }

    public static Password resetToDefault(Password annotation) {
        when(annotation.minLength()).thenReturn(8);
        when(annotation.digits()).thenReturn(0);
        when(annotation.alphabet()).thenReturn(0);
        when(annotation.lowerCase()).thenReturn(0);
        when(annotation.upperCase()).thenReturn(0);
        when(annotation.specialChars()).thenReturn(0);
        when(annotation.differentChars()).thenReturn(0);
        when(annotation.useWeakPasswordTester()).thenReturn(true);
        when(annotation.weakPasswordTester()).thenAnswer(invocation -> WeakPasswordTester.class);
        when(annotation.minLengthMessage()).thenReturn("min length message");
        when(annotation.digitsMessage()).thenReturn("digits message");
        when(annotation.alphabetMessage()).thenReturn("alphabet message");
        when(annotation.lowerCaseMessage()).thenReturn("lower case message");
        when(annotation.upperCaseMessage()).thenReturn("upper case message");
        when(annotation.specialCharsMessage()).thenReturn("special chars message");
        when(annotation.differentCharsMessage()).thenReturn("different chars message");
        when(annotation.weakPasswordMessage()).thenReturn("weak password message");
        when(annotation.specialCharsDefinition()).thenReturn(Password.DEFAULT_SPECIAL_CHARS_DEFINITION);
        return annotation;
    }

}
