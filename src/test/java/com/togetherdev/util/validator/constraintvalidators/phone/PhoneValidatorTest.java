package com.togetherdev.util.validator.constraintvalidators.phone;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberType;
import static com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberType.UAN;
import com.google.i18n.phonenumbers.Phonenumber;
import com.togetherdev.util.validator.constraints.phone.Phone;
import com.togetherdev.util.validator.util.MockConstraintValidatorContext;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PhoneValidatorTest {

    private final String DEFAULT_REGION = "BR";
    private final String NUMBER = "61 8183-4905";
    private final MockConstraintValidatorContext CONTEXT;
    private final PhoneValidatorForString INSTANCE;
    private final Phone ANNOTATION;

    public PhoneValidatorTest() {
        this.ANNOTATION = mock(Phone.class);
        this.INSTANCE = new PhoneValidatorForString();
        this.CONTEXT = MockConstraintValidatorContext.getInstance();
    }

    @Before
    public void setUp() throws Exception {
        when(ANNOTATION.defaultRegion()).thenReturn(DEFAULT_REGION);
        when(ANNOTATION.regions()).thenReturn(new String[0]);
        when(ANNOTATION.types()).thenReturn(new PhoneNumberType[0]);
        when(ANNOTATION.message()).thenReturn("Invalid phone");
        when(ANNOTATION.invalidRegionMessage()).thenReturn("Invalid region");
        when(ANNOTATION.invalidTypeMessage()).thenReturn("Invalid type");
    }

    @After
    public void tearDown() throws Exception {
        CONTEXT.clear();
    }

    @Test
    public void testWithDefaultRegion() {
        INSTANCE.initialize(ANNOTATION);
        assertTrue(INSTANCE.isValid(NUMBER, CONTEXT));
        assertTrue(CONTEXT.isEmpty());
    }

    @Test
    public void testWihoutDefaultRegion() {
        when(ANNOTATION.defaultRegion()).thenReturn("");
        INSTANCE.initialize(ANNOTATION);
        assertFalse(INSTANCE.isValid(NUMBER, CONTEXT));
        assertFalse(CONTEXT.isEmpty());
    }

    @Test
    public void testInvalidType() {
        when(ANNOTATION.types()).thenReturn(new PhoneNumberType[]{UAN});
        INSTANCE.initialize(ANNOTATION);
        assertFalse(INSTANCE.isValid(NUMBER, CONTEXT));
        assertFalse(CONTEXT.isEmpty());
    }

    @Test
    public void testValidType() throws NumberParseException {
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phone = phoneNumberUtil.parse(NUMBER, DEFAULT_REGION);
        PhoneNumberType numberType = phoneNumberUtil.getNumberType(phone);

        when(ANNOTATION.types()).thenReturn(new PhoneNumberType[]{UAN, numberType});
        INSTANCE.initialize(ANNOTATION);
        assertTrue(INSTANCE.isValid(NUMBER, CONTEXT));
        assertTrue(CONTEXT.isEmpty());
    }

    @Test
    public void testInvalidRegions() {
        when(ANNOTATION.defaultRegion()).thenReturn("PT");
        INSTANCE.initialize(ANNOTATION);

        assertFalse(INSTANCE.isValid(NUMBER, CONTEXT));
        assertFalse(CONTEXT.isEmpty());
    }

    @Test
    public void testValidRegions() {
        when(ANNOTATION.regions()).thenReturn(new String[]{"PT", DEFAULT_REGION});
        INSTANCE.initialize(ANNOTATION);

        assertTrue(INSTANCE.isValid(NUMBER, CONTEXT));
        assertTrue(CONTEXT.isEmpty());
    }

}
