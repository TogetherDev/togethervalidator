package com.togetherdev.util.validator.constraintvalidators.password;

import static com.togetherdev.util.validator.TogetherValidatorConfiguration.getResourceAsLines;
import static com.togetherdev.util.validator.constraintvalidators.password.DefaultWeakPasswordTester.WEAK_PASSWORD_VARIANTS_FILE;
import java.util.List;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

public class DefaultWeakPasswordTesterTest {

    private static List<String> weakPasswords;
    private static List<String> weakPasswordVariants;
    private static final DefaultWeakPasswordTester TESTER = DefaultWeakPasswordTester.getInstance();

    @BeforeClass
    public static void setUpClass() {
        weakPasswords = WeakPasswordCategory.getResource(WeakPasswordCategory.values()).stream().filter(line -> !line.isEmpty()).collect(toList());
        weakPasswordVariants = getResourceAsLines(WEAK_PASSWORD_VARIANTS_FILE);
    }

    @Test
    public void weakPasswordTest() {
        weakPasswords.forEach((weakPassword) -> {
            assertTrue("Weak password", TESTER.isWeakPassword(weakPassword));
        });
    }

    @Test
    public void weakPasswordWithVariantTest() {
        weakPasswords.forEach((weakPassword) -> {
            weakPasswordVariants.forEach((variant) -> {
                String weakPasswordWithVariant = (variant + weakPassword);
                String message = String.format("Weak password (%s) with prefix (%s) >>>%s<<<", weakPassword, variant, weakPasswordWithVariant);
                assertTrue(message, TESTER.isWeakPassword(weakPasswordWithVariant));

                weakPasswordWithVariant = (weakPassword + variant);
                message = String.format("Weak password (%s) with suffix (%s) >>>%s<<<", weakPassword, variant, weakPasswordWithVariant);
                assertTrue(message, TESTER.isWeakPassword(weakPasswordWithVariant));
            });
        });
    }

}
