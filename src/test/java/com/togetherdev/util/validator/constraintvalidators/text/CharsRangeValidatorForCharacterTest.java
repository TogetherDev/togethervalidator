package com.togetherdev.util.validator.constraintvalidators.text;

import com.togetherdev.util.validator.constraints.text.CharsRange;
import com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidatorTest;
import static com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidatorTest.negateValues;
import static com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidatorTest.testEachValue;
import static com.togetherdev.util.validator.util.LoggerManager.LOGGER;
import static java.lang.Character.toLowerCase;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CharsRangeValidatorForCharacterTest extends AbstractRangeValidatorTest<CharsRange, Character> {

    private static final int MIN = 'A';
    private static final int MAX = 'Z';

    public CharsRangeValidatorForCharacterTest() {
        super(new CharsRangeValidatorForCharacter(), createAnnotation());
    }

    @Override
    public void setUp() {
        when(ANNOTATION.negated()).thenReturn(false);
        when(ANNOTATION.caseSensitive()).thenReturn(true);
        when(ANNOTATION.negated()).thenReturn(false);
        initializeValues(true);
    }

    public void initializeValues(boolean caseSensitive) {
        VALUES.clear();
        for (int i = (MIN - 10); i <= (MAX + 10); i++) {
            char c = (char) i;
            VALUES.put(c, ((i >= MIN) && (i <= MAX)));
        }
        for (int i = MIN; i <= MAX; i++) {
            char c = (char) i;
            VALUES.put(toLowerCase(c), (((i >= MIN) && (i <= MAX)) && !caseSensitive));
        }
    }

    @Test
    public void insensitiveRangeTest() {
        LOGGER.info("[INSENSITIVE CASE] Testing values in and out of range...");
        when(ANNOTATION.caseSensitive()).thenReturn(false);
        VALIDATOR.initialize(ANNOTATION);
        initializeValues(false);
        testEachValue(VALUES, VALIDATOR);
    }

    @Test
    public void negatedInsensitiveRangeTest() {
        LOGGER.info("[NEGATED] [INSENSITIVE CASE] Testing if values matches or not with the predicate...");
        when(ANNOTATION.caseSensitive()).thenReturn(false);
        when(ANNOTATION.negated()).thenReturn(true);
        VALIDATOR.initialize(ANNOTATION);
        initializeValues(false);
        negateValues(VALUES);
        testEachValue(VALUES, VALIDATOR);
    }

    private static CharsRange createAnnotation() {
        CharsRange boundsAnnotation = mock(CharsRange.class);
        when(boundsAnnotation.min()).thenReturn((char) MIN);
        when(boundsAnnotation.max()).thenReturn((char) MAX);
        when(boundsAnnotation.message()).thenReturn("range message");
        return boundsAnnotation;
    }

}
