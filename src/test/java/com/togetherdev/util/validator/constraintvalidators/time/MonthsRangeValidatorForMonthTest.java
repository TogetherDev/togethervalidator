package com.togetherdev.util.validator.constraintvalidators.time;

import com.togetherdev.util.validator.constraints.time.MonthsRange;
import com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidatorTest;
import java.time.Month;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MonthsRangeValidatorForMonthTest extends AbstractRangeValidatorTest<MonthsRange, Month> {

    private static final Month MIN = Month.FEBRUARY;
    private static final Month MAX = Month.NOVEMBER;

    public MonthsRangeValidatorForMonthTest() {
        super(new MonthsRangeValidatorForMonth(), createAnnotation());
    }

    @Override
    public void setUp() {
        VALUES.clear();
        for (Month month : Month.values()) {
            VALUES.put(month, ((month.compareTo(MIN) >= 0) && (month.compareTo(MAX) <= 0)));
        }
    }

    private static MonthsRange createAnnotation() {
        MonthsRange annotation = mock(MonthsRange.class);
        when(annotation.min()).thenReturn(MIN);
        when(annotation.max()).thenReturn(MAX);
        when(annotation.message()).thenReturn("range message");
        return annotation;
    }

}
