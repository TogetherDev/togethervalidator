package com.togetherdev.util.validator.constraintvalidators.time;

import com.togetherdev.util.validator.constraints.time.WeekdaysRange;
import com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidatorTest;
import java.time.DayOfWeek;
import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.TUESDAY;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WeekdaysRangeValidatorForDayOfWeekTest extends AbstractRangeValidatorTest<WeekdaysRange, DayOfWeek> {

    private static final DayOfWeek MIN = TUESDAY;
    private static final DayOfWeek MAX = SATURDAY;

    public WeekdaysRangeValidatorForDayOfWeekTest() {
        super(new WeekdaysRangeValidatorForDayOfWeek(), createAnnotation());
    }

    @Override
    public void setUp() {
        VALUES.clear();
        for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
            VALUES.put(dayOfWeek, ((dayOfWeek.compareTo(MIN) >= 0) && (dayOfWeek.compareTo(MAX) <= 0)));
        }
    }

    private static WeekdaysRange createAnnotation() {
        WeekdaysRange annotation = mock(WeekdaysRange.class);
        when(annotation.min()).thenReturn(MIN);
        when(annotation.max()).thenReturn(MAX);
        when(annotation.message()).thenReturn("range message");
        return annotation;
    }

}
