package com.togetherdev.util.validator.constraintvalidators.number;

import com.togetherdev.util.validator.constraints.number.DecimalsRange;
import com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidatorTest;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DecimalsRangeValidatorForNumberTest extends AbstractRangeValidatorTest<DecimalsRange, Number> {

    private static final int MIN = 10;
    private static final int MAX = 20;

    public DecimalsRangeValidatorForNumberTest() {
        super(new DecimalsRangeValidatorForNumber(), createAnnotation());
    }

    @Override
    public void setUp() {
        VALUES.clear();
        for (double i = (MIN - 10); i < (MAX + 10); i++) {
            VALUES.put(i, ((i >= MIN) && (i <= MAX)));
        }
    }

    private static DecimalsRange createAnnotation() {
        DecimalsRange annotation = mock(DecimalsRange.class);
        when(annotation.min()).thenReturn((double) MIN);
        when(annotation.max()).thenReturn((double) MAX);
        when(annotation.message()).thenReturn("Range message");
        return annotation;
    }

}
