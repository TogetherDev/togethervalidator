package com.togetherdev.util.validator.constraintvalidators.text;

import com.togetherdev.util.validator.constraints.text.StartsWith;
import com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidatorTest;
import static com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidatorTest.negateValues;
import static com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidatorTest.testEachValue;
import static com.togetherdev.util.validator.util.LoggerManager.LOGGER;
import static java.lang.Character.toLowerCase;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StartsWithValidatorForCharSequenceTest extends AbstractGroupValidatorTest<StartsWith, CharSequence, String> {

    private static final int MIN = 'A';
    private static final int MAX = 'Z';

    public StartsWithValidatorForCharSequenceTest() {
        super(new StartsWithValidatorForCharSequence(), createAnnotation());
    }

    @Override
    public void setUp() {
        when(ANNOTATION.negated()).thenReturn(false);
        when(ANNOTATION.caseSensitive()).thenReturn(true);
        when(ANNOTATION.useTrimMethod()).thenReturn(false);
        when(ANNOTATION.message()).thenReturn("starts with");
        initializeValues(true);
    }

    public void initializeValues(boolean caseSensitive) {
        VALUES.clear();
        for (int i = (MIN - 10); i <= (MAX + 10); i++) {
            char c = (char) i;
            VALUES.put((c + "_suffix"), ((i >= MIN) && (i <= MAX)));
        }
        for (int i = MIN; i <= MAX; i++) {
            char c = (char) i;
            VALUES.put((toLowerCase(c) + "_suffix"), (((i >= MIN) && (i <= MAX)) && !caseSensitive));
        }
    }

    @Test
    public void insensitivePredicateTest() {
        LOGGER.info("[INSENSITIVE CASE] Testing if values matches or not with the predicate...");
        when(ANNOTATION.caseSensitive()).thenReturn(false);
        VALIDATOR.initialize(ANNOTATION);
        initializeValues(false);
        testEachValue(VALUES, VALIDATOR);
    }

    @Test
    public void negatedInsensitivePredicateTest() {
        LOGGER.info("[NEGATED] [INSENSITIVE CASE] Testing if values matches or not with the predicate...");
        when(ANNOTATION.caseSensitive()).thenReturn(false);
        when(ANNOTATION.negated()).thenReturn(true);
        VALIDATOR.initialize(ANNOTATION);
        initializeValues(false);
        negateValues(VALUES);
        testEachValue(VALUES, VALIDATOR);
    }

    private static StartsWith createAnnotation() {
        StartsWith annotation = mock(StartsWith.class);
        when(annotation.message()).thenReturn("STARTS_WITH");
        String[] values = new String[(MAX - MIN) + 1];
        for (int i = MIN; i <= MAX; i++) {
            values[(i - MIN)] = Character.toString((char) i);
        }
        when(annotation.values()).thenReturn(values);
        return annotation;
    }

}
