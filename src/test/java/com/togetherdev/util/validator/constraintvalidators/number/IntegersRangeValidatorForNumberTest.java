package com.togetherdev.util.validator.constraintvalidators.number;

import com.togetherdev.util.validator.constraints.number.IntegersRange;
import com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidatorTest;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IntegersRangeValidatorForNumberTest extends AbstractRangeValidatorTest<IntegersRange, Number> {

    private static final int MIN = 10;
    private static final int MAX = 20;

    public IntegersRangeValidatorForNumberTest() {
        super(new IntegersRangeValidatorForNumber(), createAnnotation());
    }

    @Override
    public void setUp() {
        VALUES.clear();
        for (long i = (MIN - 10); i < (MAX + 10); i++) {
            VALUES.put(i, ((i >= MIN) && (i <= MAX)));
        }
    }

    private static IntegersRange createAnnotation() {
        IntegersRange annotation = mock(IntegersRange.class);
        when(annotation.min()).thenReturn((long) MIN);
        when(annotation.max()).thenReturn((long) MAX);
        when(annotation.message()).thenReturn("Range message");
        return annotation;
    }

}
