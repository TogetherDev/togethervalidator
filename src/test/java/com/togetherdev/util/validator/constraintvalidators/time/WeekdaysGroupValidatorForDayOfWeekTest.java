package com.togetherdev.util.validator.constraintvalidators.time;

import com.togetherdev.util.validator.constraints.time.WeekdaysGroup;
import com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidatorTest;
import java.time.DayOfWeek;
import static java.time.DayOfWeek.SATURDAY;
import static java.time.DayOfWeek.TUESDAY;
import java.util.Arrays;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WeekdaysGroupValidatorForDayOfWeekTest extends AbstractGroupValidatorTest<WeekdaysGroup, DayOfWeek, DayOfWeek> {

    private static final DayOfWeek MIN = TUESDAY;
    private static final DayOfWeek MAX = SATURDAY;

    public WeekdaysGroupValidatorForDayOfWeekTest() {
        super(new WeekdaysGroupValidatorForDayOfWeek(), createAnnotation());
    }

    @Override
    public void setUp() {
        VALUES.clear();
        for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
            VALUES.put(dayOfWeek, ((dayOfWeek.compareTo(MIN) >= 0) && (dayOfWeek.compareTo(MAX) <= 0)));
        }
    }

    private static WeekdaysGroup createAnnotation() {
        WeekdaysGroup annotation = mock(WeekdaysGroup.class);
        when(annotation.message()).thenReturn("group message");
        DayOfWeek[] days = Arrays.copyOfRange(DayOfWeek.values(), MIN.ordinal(), (MAX.ordinal() + 1));
        when(annotation.days()).thenReturn(days);
        return annotation;
    }

}
