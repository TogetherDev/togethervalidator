package com.togetherdev.util.validator.constraintvalidators;

import static com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidatorTest.negateValues;
import static com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidatorTest.testEachValue;
import static com.togetherdev.util.validator.util.LoggerManager.LOGGER;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;
import static java.util.Objects.requireNonNull;
import org.junit.Before;
import org.junit.Test;

public abstract class AbstractGroupValidatorTest<A extends Annotation, X, Y> {

    protected final A ANNOTATION;
    protected final Map<X, Boolean> VALUES;
    protected final AbstractGroupValidator<A, X, Y> VALIDATOR;

    public AbstractGroupValidatorTest(AbstractGroupValidator<A, X, Y> validator, A annotation) {
        this.ANNOTATION = requireNonNull(annotation);
        this.VALIDATOR = requireNonNull(validator);
        this.VALUES = new HashMap<>();
    }

    @Before
    public abstract void setUp();

    @Test
    public void groupTest() {
        LOGGER.info("Testing values in and out of the group...");
        VALIDATOR.initialize(ANNOTATION);
        testEachValue(VALUES, VALIDATOR);
    }

    @Test
    public void negatedGroupTest() {
        LOGGER.info("[NEGATED] Testing values in and out of the group...");
        VALIDATOR.initialize(ANNOTATION);
        VALIDATOR.negated = true;
        negateValues(VALUES);
        testEachValue(VALUES, VALIDATOR);
    }

}
