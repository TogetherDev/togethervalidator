package com.togetherdev.util.validator.constraintvalidators.number;

import com.togetherdev.util.validator.constraints.number.IntegersGroup;
import com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidatorTest;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class IntegersGroupValidatorForNumberTest extends AbstractGroupValidatorTest<IntegersGroup, Number, Number> {

    private static final int MIN = 10;
    private static final int MAX = 20;

    public IntegersGroupValidatorForNumberTest() {
        super(new IntegersGroupValidatorForNumber(), createAnnotation());
    }

    @Override
    public void setUp() {
        VALUES.clear();
        for (long i = (MIN - 10); i < (MAX + 10); i++) {
            VALUES.put(i, ((i >= MIN) && (i <= MAX)));
        }
    }

    private static IntegersGroup createAnnotation() {
        IntegersGroup annotation = mock(IntegersGroup.class);
        when(annotation.message()).thenReturn("Range message");
        long[] values = new long[(MAX - MIN) + 1];
        for (int i = MIN; i <= MAX; i++) {
            values[i - MIN] = i;
        }
        when(annotation.values()).thenReturn(values);
        return annotation;
    }

}
