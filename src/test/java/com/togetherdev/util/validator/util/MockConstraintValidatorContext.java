package com.togetherdev.util.validator.util;

import javax.validation.ConstraintValidatorContext;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract interface MockConstraintValidatorContext extends ConstraintValidatorContext, ConstraintValidatorContext.ConstraintViolationBuilder {

    public static MockConstraintValidatorContext getInstance() {
        MockConstraintValidatorContext mock = mock(MockConstraintValidatorContext.class);
        doNothing().when(mock).disableDefaultConstraintViolation();
        when(mock.getDefaultConstraintMessageTemplate()).thenReturn("");
        when(mock.addConstraintViolation()).thenReturn(mock);
        when(mock.addConstraintViolation()).thenReturn(mock);
        StringBuilder builder = new StringBuilder();
        when(mock.isEmpty()).thenAnswer(invocation -> builder.length() == 0);
        when(mock.buildConstraintViolationWithTemplate(anyString())).then(invocation -> {
            String messageTemplate = invocation.getArgument(0);
            builder.append(messageTemplate);
            return mock;
        });
        doAnswer(invocation -> {
            builder.setLength(0);
            return null;
        }).when(mock).clear();
        return mock;
    }

    public boolean isEmpty();

    public void clear();

}
