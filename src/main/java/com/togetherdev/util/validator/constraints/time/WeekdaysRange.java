package com.togetherdev.util.validator.constraints.time;

import com.togetherdev.util.validator.constraintvalidators.time.WeekdaysRangeValidatorForDayOfWeek;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import java.time.DayOfWeek;
import java.time.format.TextStyle;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * The annotated element must be a month between the defined months. Accepts {@linkplain java.time.DayOfWeek} type.
 *
 * @author Thomás Sousa Silva
 */
@Documented
@Retention(RUNTIME)
@Constraint(validatedBy = WeekdaysRangeValidatorForDayOfWeek.class)
@Target(value = {METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
public @interface WeekdaysRange {

    /**
     * The message that is used when the validated weekday is out of the bounds.
     *
     * @return The bounds message.
     */
    String message() default "{com.togetherdev.validator.constraints.time.WeekdaysRange.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * The minimum valid weekday (inclusive).
     *
     * @return The minimum valid weekday.
     * @throws java.lang.IllegalArgumentException If this day is larger than maximum day.
     */
    DayOfWeek min() default DayOfWeek.MONDAY;

    /**
     * The maximum valid weekday (inclusive).
     *
     * @return The maximum valid weekday.
     */
    DayOfWeek max() default DayOfWeek.SUNDAY;

    /**
     * Use true to indicate that the validator must negate the validation result.
     *
     * @return True if is negated.
     */
    boolean negated() default false;

    /**
     * The style that will be use to format the weekdays, if and only if the message have a key in the {@linkplain com.togetherdev.util.validator.messageinterpolation.MessageAttributeInterpolator#KEY_PATTERN TogetherValidator Key Pattern}, that is "[min]" and "[max]".
     *
     * @return The style.
     */
    TextStyle style() default TextStyle.FULL;

}
