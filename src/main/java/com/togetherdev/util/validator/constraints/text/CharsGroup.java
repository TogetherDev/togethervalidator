package com.togetherdev.util.validator.constraints.text;

import com.togetherdev.util.validator.constraintvalidators.text.CharsGroupValidatorForCharacter;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * * The annotated element must be equal to at least one value of the defined group. Accepts {@linkplain Character} type.
 *
 * @author Thomás Sousa Silva
 */
@Documented
@Retention(RUNTIME)
@Constraint(validatedBy = CharsGroupValidatorForCharacter.class)
@Target(value = {METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
public @interface CharsGroup {

    /**
     * The message that is used when the validated value is different of all group values.
     *
     * @return The group message.
     */
    String message() default "{com.togetherdev.validator.constraints.text.CharsGroup.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return The valid chars.
     */
    char[] values();

    /**
     * Use true to indicate that the validator must be case sensitive.
     *
     * @return True if is case sensitive.
     */
    boolean caseSensitive() default true;

    /**
     * Use true to indicate that the validator must negate the validation result.
     *
     * @return True if is negated.
     */
    boolean negated() default false;

}
