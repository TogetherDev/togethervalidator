package com.togetherdev.util.validator.constraints.time;

import com.togetherdev.util.format.time.StandardDateTimeFormatter;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForInstant;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForLocalDate;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForLocalDateTime;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForLocalTime;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForOffsetDateTime;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForOffsetTime;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForYear;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForYearMonth;
import com.togetherdev.util.validator.constraintvalidators.time.FutureValidators.FutureValidatorForZonedDateTime;
import com.togetherdev.util.validator.util.UserZoneIdProvider;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import java.time.temporal.ChronoUnit;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * <p>
 * The annotated element must be in the future.</p>
 * <table style="border: 1px solid black;">
 * <caption><h2>Accepted types</h2></caption>
 * <thead>
 * <tr>
 * <th align="center" style="border: 1px solid black;">Type</th>
 * <th align="center" style="border: 1px solid black;">Uses the {@linkplain #defaultZoneIdProvider() default ZoneId provider}</th>
 * </tr>
 * </thead>
 * <tbody>
 * <tr>
 * <td align="center" style="border: 1px solid black;">{@linkplain java.time.Instant}</td>
 * <td align="center" style="border: 1px solid black;">Yes</td>
 * </tr>
 * <tr>
 * <td align="center" style="border: 1px solid black;">{@linkplain java.time.LocalDate}</td>
 * <td align="center" style="border: 1px solid black;">Yes</td>
 * </tr>
 * <tr>
 * <td align="center" style="border: 1px solid black;">{@linkplain java.time.LocalDateTime}</td>
 * <td align="center" style="border: 1px solid black;">Yes</td>
 * </tr>
 * <tr>
 * <td align="center" style="border: 1px solid black;">{@linkplain java.time.LocalTime}</td>
 * <td align="center" style="border: 1px solid black;">Yes</td>
 * </tr>
 * <tr>
 * <td align="center" style="border: 1px solid black;">{@linkplain java.time.Year}</td>
 * <td align="center" style="border: 1px solid black;">Yes</td>
 * </tr>
 * <tr>
 * <td align="center" style="border: 1px solid black;">{@linkplain java.time.YearMonth}</td>
 * <td align="center" style="border: 1px solid black;">Yes</td>
 * </tr>
 * <tr>
 * <td align="center" style="border: 1px solid black;">{@linkplain java.time.ZonedDateTime}</td>
 * <td align="center" style="border: 1px solid black;">No</td>
 * </tr>
 * <tr>
 * <td align="center" style="border: 1px solid black;">{@linkplain java.time.OffsetDateTime}</td>
 * <td align="center" style="border: 1px solid black;">No</td>
 * </tr>
 * <tr>
 * <td align="center" style="border: 1px solid black;">{@linkplain java.time.OffsetTime}</td>
 * <td align="center" style="border: 1px solid black;">No</td>
 * </tr>
 * </tbody>
 * <footer>
 * <tr>
 * <td colspan="2" style="border: 1px solid black;"><b>Note</b>: The validators of this types provides the {@linkplain com.togetherdev.util.validator.constraintvalidation.ValidationContext dynamic attributes} "<b>[minLimit]</b>" and "<b>[maxLimit]</b>".
 * This attributes are the sum result of current time + {@linkplain #minDifference() minimum} and {@linkplain #maxDifference() maximum} difference, respectively.</td>
 * </tr>
 * </footer>
 * </table>
 *
 * @author Thomás Sousa Silva
 */
@Documented
@Retention(RUNTIME)
@Constraint(validatedBy = {
    FutureValidatorForYear.class,
    FutureValidatorForInstant.class,
    FutureValidatorForYearMonth.class,
    FutureValidatorForLocalTime.class,
    FutureValidatorForLocalDate.class,
    FutureValidatorForLocalDateTime.class,
    FutureValidatorForZonedDateTime.class,
    FutureValidatorForOffsetDateTime.class,
    FutureValidatorForOffsetTime.class
})
@Target(value = {METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
public @interface Future {

    /**
     * The message that is used when the validated value is not in the future.
     *
     * @return The future message.
     */
    String message() default "{com.togetherdev.validator.constraints.time.Future.message}";

    /**
     * The message that is used when the validated value is beyond of the maximum limit (current time + {@linkplain #maxDifference() maximum difference}).
     *
     * @return The maximum limit message.
     */
    String maxLimitMessage() default "{com.togetherdev.validator.constraints.time.Future.maxLimitMessage}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * <p>
     * The default zone ID provider that will be used to get the current time, <b>only if the validated type have not a zone id.</b></p>
     * <div>
     * Note:
     * <ul>
     * <li>The provider must have a public constructor without parameters.</li>
     * <li>If the given class is the {@linkplain UserZoneIdProvider} class, then the provider will be the {@link UserZoneIdProvider#UTC_PROVIDER UTC provider}, it always will provide the {@linkplain java.time.ZoneOffset#UTC UTC zone}.</li>
     * </ul>
     * </div>
     *
     * @return The default zone ID provider.
     */
    Class<? extends UserZoneIdProvider> defaultZoneIdProvider() default UserZoneIdProvider.class;

    /**
     * True indicates that a time equals current time is considered valid.
     *
     * @return True if the current time is considered valid.
     */
    boolean currentTimeValid() default false;

    /**
     * <p>
     * The minimum difference to the current time (inclusive).</p>
     * <p>
     * Note: If less or equal to zero it will be ignored.</p>
     *
     * @return The maximum difference.
     */
    long minDifference() default 0;

    /**
     * The unit type of the minimum difference.
     *
     * @return The unit type.
     */
    ChronoUnit minDifferenceUnit() default ChronoUnit.MILLIS;

    /**
     * <p>
     * The maximum difference to the current time (inclusive).</p>
     * <p>
     * Note: If less or equal to zero it will be ignored.</p>
     *
     * @return The maximum difference.
     */
    long maxDifference() default 0;

    /**
     * The unit type of the maximum difference.
     *
     * @return The unit type.
     */
    ChronoUnit maxDifferenceUnit() default ChronoUnit.MILLIS;

    /**
     * <p>
     * The pattern that will be used to format the time attributes, if and only if the message have a key in the {@linkplain com.togetherdev.util.validator.messageinterpolation.MessageAttributeInterpolator#KEY_PATTERN TogetherValidator Key Pattern}.</p>
     * <p>
     * Note: If empty will be used the standard date time formatter.</p>
     *
     * @return The pattern.
     * @see java.time.format.DateTimeValidatorFormatter#ofPattern(java.lang.String) ofPattern
     */
    String pattern() default "";

    /**
     * <p>
     * The standard date time formatter is used to format the time attributes, if and only if the message have a key in the {@linkplain com.togetherdev.util.validator.messageinterpolation.MessageAttributeInterpolator#KEY_PATTERN TogetherValidator Key Pattern}.</p>
     * <p>
     * Note: This formatter <b>only will be used if the {@linkplain #pattern() pattern} is empty</b>.</p>
     *
     * @return The formatter.
     */
    StandardDateTimeFormatter standardFormatter() default StandardDateTimeFormatter.TO_STRING;

}
