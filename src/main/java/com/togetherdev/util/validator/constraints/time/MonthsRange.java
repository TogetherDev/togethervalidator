package com.togetherdev.util.validator.constraints.time;

import com.togetherdev.util.validator.constraintvalidators.time.MonthsRangeValidatorForMonth;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import java.time.Month;
import java.time.format.TextStyle;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * The annotated element must be a month between the defined months. Accepts {@linkplain java.time.Month} type.
 *
 * @author Thomás Sousa Silva
 */
@Documented
@Retention(RUNTIME)
@Constraint(validatedBy = MonthsRangeValidatorForMonth.class)
@Target(value = {METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
public @interface MonthsRange {

    /**
     * The message that is used when the validated month is out of the bounds.
     *
     * @return The bounds message.
     */
    String message() default "{com.togetherdev.validator.constraints.time.MonthsRange.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * The minimum valid month (inclusive).
     *
     * @return The minimum valid month.
     * @throws java.lang.IllegalArgumentException If this month is larger than maximum month.
     */
    Month min() default Month.JANUARY;

    /**
     * The maximum valid month (inclusive).
     *
     * @return The maximum valid month.
     */
    Month max() default Month.DECEMBER;

    /**
     * Use true to indicate that the validator must negate the validation result.
     *
     * @return True if is negated.
     */
    boolean negated() default false;

    /**
     * The style that will be use to format the months, if and only if the message have a key in the {@linkplain com.togetherdev.util.validator.messageinterpolation.MessageAttributeInterpolator#KEY_PATTERN TogetherValidator Key Pattern}, that is "[min]" and "[max]".
     *
     * @return The style.
     */
    TextStyle style() default TextStyle.FULL;

}
