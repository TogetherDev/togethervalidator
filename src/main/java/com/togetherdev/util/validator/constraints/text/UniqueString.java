package com.togetherdev.util.validator.constraints.text;

import com.togetherdev.util.validator.constraintvalidators.text.UniqueStringValidatorForString;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * The annotated element must be a month between the defined months. Accepts {@linkplain java.time.Month} type.
 *
 * @author Thomás Sousa Silva
 */
@Documented
@Retention(RUNTIME)
@Constraint(validatedBy = UniqueStringValidatorForString.class)
@Target(value = {METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
public @interface UniqueString {

    String message() default "{com.togetherdev.validator.constraints.text.UniqueString.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * <p>
     * The unique string tester tester that will be used to test if the values is unique.</p>
     * <p>
     * Note: The tester must have a public constructor without parameters.</p>
     *
     * @return The unique string tester.
     */
    Class<? extends UniqueStringTester> tester();

    /**
     * <p>
     * Represents an unique string tester. A unique string tester should check if a given value is unique.</p>
     * <p>
     * Note: The definition of unique is defined by the implementation.</p>
     */
    public static interface UniqueStringTester {

        /**
         * Tests if the given value is unique.
         *
         * @param value The value that will be tested.
         * @return True if the given value is unique.
         */
        public boolean isUnique(String value);

    }

}
