package com.togetherdev.util.validator.constraints.phone;

import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberType;
import com.togetherdev.util.validator.constraintvalidators.phone.PhoneValidatorForPhoneNumber;
import com.togetherdev.util.validator.constraintvalidators.phone.PhoneValidatorForString;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * <p>
 * The annotated element must be a valid phone number.</p>
 * <div>
 * Accepts the bellow types:
 * <ul>
 * <li>{@linkplain String}</li>
 * <li>{@linkplain com.google.i18n.phonenumbers.PhoneNumber}</li>
 * </ul>
 * </div>
 *
 * @author Thomás Sousa Silva
 */
@Documented
@Retention(RUNTIME)
@Constraint(validatedBy = {
    PhoneValidatorForString.class,
    PhoneValidatorForPhoneNumber.class
})
@Target(value = {METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
public @interface Phone {

    /**
     * The message that is used when the validated number is invalid.
     *
     * @return The invalid number message.
     */
    String message() default "{com.togetherdev.validator.constraints.phone.Phone.message}";

    /**
     * The message that is used when the validated number is of a type that different of all {@link #types() valid number types}.
     *
     * @return The invalid type message.
     */
    String invalidTypeMessage() default "{com.togetherdev.validator.constraints.phone.Phone.invalidTypeMessage}";

    /**
     * The message that is used when the validated number is of a region that different of all {@link #regions() valid regions}.
     *
     * @return The invalid region message.
     */
    String invalidRegionMessage() default "{com.togetherdev.validator.constraints.phone.Phone.invalidRegionMessage}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * The default region that is used to {@linkplain com.google.i18n.phonenumbers.PhoneNumberUtil#parse(java.lang.String, java.lang.String) parse}.
     *
     * @return The default region.
     */
    String defaultRegion() default "";

    /**
     * <p>
     * The valid regions that is used to {@linkplain com.google.i18n.phonenumbers.PhoneNumberUtil#isValidNumberForRegion(com.google.i18n.phonenumbers.Phonenumber.PhoneNumber, java.lang.String) validate} the number.</p>
     * <p>
     * Note: If no regions is defined, then {@linkplain com.google.i18n.phonenumbers.PhoneNumberUtil#isValidNumber(com.google.i18n.phonenumbers.Phonenumber.PhoneNumber) valid numbers} of any region will be considered valid.</p>
     *
     * @return The valid regions.
     */
    String[] regions() default {};

    /**
     * <p>
     * The valid types that is used to {@linkplain com.google.i18n.phonenumbers.PhoneNumberUtil#getNumberType(com.google.i18n.phonenumbers.Phonenumber.PhoneNumber) validate} the number.</p>
     * <p>
     * Note: If no types is defined, then valid numbers of any type will be considered valid.</p>
     *
     * @return The valid types.
     */
    PhoneNumberType[] types() default {};

}
