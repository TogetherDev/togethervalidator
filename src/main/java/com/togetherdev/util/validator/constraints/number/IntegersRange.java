package com.togetherdev.util.validator.constraints.number;

import com.togetherdev.util.validator.constraintvalidators.number.IntegersRangeValidatorForNumber;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * The annotated element must be a numbers between the defined numbers. Accepts {@linkplain Number} type.
 *
 * @author Thomás Sousa Silva
 */
@Documented
@Retention(RUNTIME)
@Constraint(validatedBy = IntegersRangeValidatorForNumber.class)
@Target(value = {METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
public @interface IntegersRange {

    /**
     * The message that is used when the validated value is out of the range.
     *
     * @return The message.
     */
    String message() default "{com.togetherdev.validator.constraints.number.IntegersRange.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * The minimum valid value (inclusive).
     *
     * @return The minimum valid value.
     * @throws java.lang.IllegalArgumentException If this value is larger than maximum value.
     */
    long min() default 0;

    /**
     * The maximum valid value (inclusive).
     *
     * @return The maximum valid value.
     */
    long max() default Long.MAX_VALUE;

    /**
     * Use true to indicate that the validator must negate the validation result.
     *
     * @return True if is negated.
     */
    boolean negated() default false;

    /**
     * <p>
     * The format that will be use to format the values, if and only if the message have a key in the {@linkplain com.togetherdev.util.validator.messageinterpolation.MessageAttributeInterpolator#KEY_PATTERN TogetherValidator Key Pattern}, that is "[min]" and "[max]".</p>
     * <p>
     * Note: If this format is defined, then will be used the {@linkplain String#format(java.util.Locale, java.lang.String, java.lang.Object...) format method}, else will be used the {@linkplain java.text.NumberFormat#getInstance(java.util.Locale) number formatter}.</p>
     *
     * @return The format.
     */
    String format() default "";

}
