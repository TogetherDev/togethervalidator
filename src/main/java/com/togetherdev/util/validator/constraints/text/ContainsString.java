package com.togetherdev.util.validator.constraints.text;

import com.togetherdev.util.validator.constraintvalidators.text.ContainsStringValidatorForCharSequence;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * The annotated element must contain one of the defined values. Accepts {@linkplain CharSequence} type.
 *
 * @author Thomás Sousa Silva
 */
@Documented
@Retention(RUNTIME)
@Constraint(validatedBy = ContainsStringValidatorForCharSequence.class)
@Target(value = {METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
public @interface ContainsString {

    String message() default "{com.togetherdev.validator.constraints.text.ContainsString.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return The valid values.
     */
    String[] values();

    /**
     * Use true to indicate that the validator must be case sensitive.
     *
     * @return True if is case sensitive.
     */
    boolean caseSensitive() default true;

    /**
     * Use true to indicate that the validator must use the {@linkplain String#trim() trim method}.
     *
     * @return True if use trim method.
     */
    boolean useTrimMethod() default false;

    /**
     * Use true to indicate that the validator must negate the validation result.
     *
     * @return True if is negated.
     */
    boolean negated() default false;

    /**
     * <p>
     * The format that will be use to format the values, if and only if the message have a key in the {@linkplain com.togetherdev.util.validator.messageinterpolation.MessageAttributeInterpolator#KEY_PATTERN TogetherValidator Key Pattern}, that is "[values]".</p>
     * <p>
     * Note: If this format is empty, then will be used the {@linkplain String#toString() toString method}, else will be used the {@linkplain String#format(java.util.Locale, java.lang.String, java.lang.Object...) format method} with the given format.</p>
     *
     * @return The format.
     */
    String format() default "";

}
