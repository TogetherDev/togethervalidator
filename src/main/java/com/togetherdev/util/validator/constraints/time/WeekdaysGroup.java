package com.togetherdev.util.validator.constraints.time;

import com.togetherdev.util.validator.constraintvalidators.time.WeekdaysGroupValidatorForDayOfWeek;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import java.time.DayOfWeek;
import java.time.format.TextStyle;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * The annotated element must be equal to at least one weekday of the defined group. Accepts {@linkplain java.time.DayOfWeek} type.
 *
 * @author Thomás Sousa Silva
 */
@Documented
@Retention(RUNTIME)
@Constraint(validatedBy = WeekdaysGroupValidatorForDayOfWeek.class)
@Target(value = {METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
public @interface WeekdaysGroup {

    /**
     * The message that is used when the validated weekday is different of all group values.
     *
     * @return The group message.
     */
    String message() default "{com.togetherdev.validator.constraints.time.WeekdaysGroup.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return The valid days.
     */
    DayOfWeek[] days();

    /**
     * Use true to indicate that the validator must negate the validation result.
     *
     * @return True if is negated.
     */
    boolean negated() default false;

    /**
     * The style that will be use to format the weekdays, if and only if the message have a key in the {@linkplain com.togetherdev.util.validator.messageinterpolation.MessageAttributeInterpolator#KEY_PATTERN TogetherValidator Key Pattern}, that is "[values]".
     *
     * @return The style.
     */
    TextStyle style() default TextStyle.FULL;

}
