package com.togetherdev.util.validator.constraints.text;

import com.togetherdev.util.validator.constraintvalidators.text.CharsRangeValidatorForCharacter;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * The annotated element must be a char between the defined chars. Accepts {@linkplain Character} type.
 *
 * @author Thomás Sousa Silva
 */
@Documented
@Retention(RUNTIME)
@Constraint(validatedBy = CharsRangeValidatorForCharacter.class)
@Target(value = {METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
public @interface CharsRange {

    /**
     * The message that is used when the validated value is out of the bounds.
     *
     * @return The bounds message.
     */
    String message() default "{com.togetherdev.validator.constraints.text.CharsRange.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * The minimum valid char (inclusive).
     *
     * @return The minimum valid char.
     * @throws java.lang.IllegalArgumentException If this char is larger than maximum char.
     */
    char min() default Character.MIN_VALUE;

    /**
     * The maximum valid char (inclusive).
     *
     * @return The maximum valid char.
     */
    char max() default Character.MAX_VALUE;

    /**
     * Use true to indicate that the validator must be case sensitive.
     *
     * @return True if is case sensitive.
     */
    boolean caseSensitive() default true;

    /**
     * Use true to indicate that the validator must negate the validation result.
     *
     * @return True if is negated.
     */
    boolean negated() default false;

}
