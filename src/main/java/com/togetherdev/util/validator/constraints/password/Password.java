package com.togetherdev.util.validator.constraints.password;

import com.togetherdev.util.validator.constraintvalidators.password.DefaultWeakPasswordTester;
import com.togetherdev.util.validator.constraintvalidators.password.PasswordValidatorForString;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * The annotated element must be a valid password. Accepts {@linkplain String} type.
 *
 * @author Thomás Sousa Silva
 */
@Documented
@Retention(RUNTIME)
@Constraint(validatedBy = PasswordValidatorForString.class)
@Target(value = {METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
public @interface Password {

    /**
     * <p>
     * The characters that is considered special by default.</p>
     * <p>
     * {@value #DEFAULT_SPECIAL_CHARS_DEFINITION}</p>
     */
    public static final String DEFAULT_SPECIAL_CHARS_DEFINITION = " !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";

    /**
     * Message without use. Use the specific messages.
     *
     * @return The message.
     * @see #minLengthMessage() minLengthMessage
     * @see #digitsMessage() digitsMessage
     * @see #alphabetMessage() alphabetMessage
     * @see #lowerCaseMessage() lowerCaseMessage
     * @see #upperCaseMessage() upperCaseMessage
     * @see #specialCharsMessage() specialCharsMessage
     * @see #differentCharsMessage() differentCharsMessage
     * @see #weakPasswordMessage() weakPasswordMessage
     */
    String message() default "";

    /**
     * The message that is used when the validated password have not a minimum length.
     *
     * @return The minimum length message.
     */
    String minLengthMessage() default "{com.togetherdev.validator.constraints.password.Password.minLengthMessage}";

    /**
     * The message that is used when the validated password have not the {@link #digits() minimum digits amount}.
     *
     * @return The required digits message.
     */
    String digitsMessage() default "{com.togetherdev.validator.constraints.password.Password.digitsMessage}";

    /**
     * The message that is used when the validated password have not the {@link #alphabet() minimum alphabet amount}.
     *
     * @return The required alphabet message.
     */
    String alphabetMessage() default "{com.togetherdev.validator.constraints.password.Password.alphabetMessage}";

    /**
     * The message that is used when the validated password have not the {@link #lowerCase() minimum lower case letters amount}.
     *
     * @return The required lower case message.
     */
    String lowerCaseMessage() default "{com.togetherdev.validator.constraints.password.Password.lowerCaseMessage}";

    /**
     * The message that is used when the validated password have not the {@link #upperCase() minimum upper case letters amount}.
     *
     * @return The required upper case message.
     */
    String upperCaseMessage() default "{com.togetherdev.validator.constraints.password.Password.upperCaseMessage}";

    /**
     * The message that is used when the validated password have not the {@link #specialChars() minimum special chars amount}.
     *
     * @return The required special chars message.
     */
    String specialCharsMessage() default "{com.togetherdev.validator.constraints.password.Password.specialCharsMessage}";

    /**
     * The message that is used when the validated password have not the {@link #differentChars() minimum different chars amount}.
     *
     * @return The different chars message.
     */
    String differentCharsMessage() default "{com.togetherdev.validator.constraints.password.Password.differentCharsMessage}";

    /**
     * The message that is used when the validated password is a {@linkplain #weakPasswordTester() weak password}.
     *
     * @return The weak password message.
     */
    String weakPasswordMessage() default "{com.togetherdev.validator.constraints.password.Password.weakPasswordMessage}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * The minimum length that a password should have.
     *
     * @return The minimum length.
     */
    int minLength() default 8;

    /**
     * The minimum {@linkplain Character#isDigit(char) digits} amount.
     *
     * @return The minimum digits amount.
     */
    int digits() default 0;

    /**
     * The minimum {@linkplain Character#isAlphabetic(int) alphabet} amount.
     *
     * @return The minimum alphabet amount.
     */
    int alphabet() default 0;

    /**
     * The minimum {@linkplain Character#isLowerCase(char) lower case letters} amount.
     *
     * @return The minimum lower case letters amount.
     */
    int lowerCase() default 0;

    /**
     * The minimum {@linkplain Character#isUpperCase(char) upper case letters} amount.
     *
     * @return The minimum upper case letters amount.
     */
    int upperCase() default 0;

    /**
     * The minimum special chars amount.
     *
     * @return The minimum special chars amount.
     * @see #specialCharsDefinition()specialCharsDefinition
     */
    int specialChars() default 0;

    /**
     * <p>
     * The minimum different chars amount.</p>
     * <p>
     * It is useful for prevent that use password composed by one single char. Example: "111111". If this difference is equals two, then the passwords must be composed by at least two chars, by example: "11112222". If it equals three require three chars, and so on.</p>
     * <p>
     * Note: If it is less or equal to one, then it will be ignored.</p>
     *
     * @return The minimum different chars amount.
     */
    int differentChars() default 3;

    /**
     * The characters that is considered special.
     *
     * @return The special chars.
     * @see #specialChars() specialChars
     * @see #DEFAULT_SPECIAL_CHARS_DEFINITION default special chars definition
     *
     */
    String specialCharsDefinition() default DEFAULT_SPECIAL_CHARS_DEFINITION;

    /**
     * Use true to indicates that should use the {@linkplain #weakPasswordTester() weak password tester}.
     *
     * @return True to indicates that should use the tester.
     */
    boolean useWeakPasswordTester() default true;

    /**
     * <p>
     * The weak password tester that will be used to test if the passwords is weak.</p>
     * <div>
     * Note:
     * <ul>
     * <li>This tester is used if and only if the {@linkplain #useWeakPasswordTester() useWeakPasswordTester method} returns true.</li>
     * <li>This tester is used if and only if the given password have all others valid password constraints.</li>
     * <li>The tester must have a public constructor without parameters.</li>
     * <li>If the given class is the {@linkplain WeakPasswordTester} or {@linkplain DefaultWeakPasswordTester} class, then the tester will be the {@link DefaultWeakPasswordTester#getInstance() default password tester}.</li>
     * </ul>
     * </div>
     *
     * @return The default zone ID provider.
     */
    Class<? extends WeakPasswordTester> weakPasswordTester() default DefaultWeakPasswordTester.class;

    /**
     * <p>
     * Represents a weak password tester. A weak password tester should check if a given password is weak.</p>
     * <p>
     * Note: The definition of weak and strong is defined by the implementation.</p>
     */
    public interface WeakPasswordTester {

        /**
         * Tests if the given password is weak.
         *
         * @param password The password that will be tested.
         * @return True if the given password is weak.
         */
        public boolean isWeakPassword(String password);

    }

}
