package com.togetherdev.util.validator.util;

import java.time.ZoneId;
import static java.time.ZoneOffset.UTC;

public interface UserZoneIdProvider {

    /**
     * Always provides {@linkplain java.time.ZoneOffset#UTC UTC Zone}.
     */
    public static final UserZoneIdProvider UTC_PROVIDER = () -> UTC;

    public ZoneId getUserZoneId();

}
