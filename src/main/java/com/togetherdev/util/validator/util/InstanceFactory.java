package com.togetherdev.util.validator.util;

import com.togetherdev.util.validator.constraints.password.Password.WeakPasswordTester;
import com.togetherdev.util.validator.constraintvalidators.password.DefaultWeakPasswordTester;
import static com.togetherdev.util.validator.util.UserZoneIdProvider.UTC_PROVIDER;

public final class InstanceFactory {

    public static <T> T newInstance(Class<T> c) {
        try {
            return c.newInstance();
        } catch (InstantiationException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static UserZoneIdProvider newZoneIdProvider(Class<? extends UserZoneIdProvider> providerClass) {
        return (providerClass.equals(UserZoneIdProvider.class) ? UTC_PROVIDER : newInstance(providerClass));
    }

    public static WeakPasswordTester newWeakPasswordTester(Class<? extends WeakPasswordTester> testerClass) {
        return ((testerClass.equals(WeakPasswordTester.class) || testerClass.equals(DefaultWeakPasswordTester.class)) ? DefaultWeakPasswordTester.getInstance() : newInstance(testerClass));
    }

}
