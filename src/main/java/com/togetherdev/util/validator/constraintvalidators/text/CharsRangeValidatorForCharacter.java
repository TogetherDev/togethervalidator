package com.togetherdev.util.validator.constraintvalidators.text;

import com.togetherdev.util.validator.constraints.text.CharsRange;
import com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidator;
import static java.lang.Character.toLowerCase;

public class CharsRangeValidatorForCharacter extends AbstractRangeValidator<CharsRange, Character> {

    @Override
    public void initialize(CharsRange annotation) {
        char min = annotation.min(), max = annotation.max();
        if (annotation.caseSensitive()) {
            this.comparator = Character::compare;
        } else {
            max = toLowerCase(max);
            min = toLowerCase(min);
            this.comparator = (x, y) -> Character.compare(toLowerCase(x), y);
        }
        this.negated = annotation.negated();
        super.initialize(min, max, annotation.message());
    }

}
