package com.togetherdev.util.validator.constraintvalidators.number;

import com.togetherdev.util.validator.constraints.number.DecimalsRange;
import com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidator;

public class DecimalsRangeValidatorForNumber extends AbstractRangeValidator<DecimalsRange, Number> {

    public DecimalsRangeValidatorForNumber() {
        super((n1, n2) -> Double.compare(n1.doubleValue(), n2.doubleValue()));
    }

    @Override
    public void initialize(DecimalsRange annotation) {
        this.negated = annotation.negated();
        super.initialize(annotation.min(), annotation.max(), annotation.message());
    }

}
