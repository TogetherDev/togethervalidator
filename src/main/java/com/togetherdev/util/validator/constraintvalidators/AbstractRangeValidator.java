package com.togetherdev.util.validator.constraintvalidators;

import static com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidator.isTrue;
import java.lang.annotation.Annotation;
import java.util.Comparator;
import static java.util.Objects.requireNonNull;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * This class is a bounds validator.
 *
 * @param <A> The annotation type.
 * @param <T> The type of object that will be validated.
 * @author Thomás Sousa Silva
 */
public abstract class AbstractRangeValidator<A extends Annotation, T> implements ConstraintValidator<A, T> {

    private T min;
    private T max;
    private String message;
    protected boolean negated;
    protected Comparator<? super T> comparator;

    public AbstractRangeValidator() {
    }

    public AbstractRangeValidator(Comparator<? super T> comparator) {
        this.comparator = comparator;
    }

    protected void initialize(T min, T max, String message) {
        if (comparator.compare(min, max) > 0) {
            throw new IllegalArgumentException(String.format("The min [%s] is larger than the max [%s].", min, max));
        }
        this.min = min;
        this.max = max;
        this.message = requireNonNull(message);
    }

    @Override
    public boolean isValid(T value, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        boolean valid = ((value == null)
                || (((comparator.compare(value, min) >= 0) && (comparator.compare(value, max) <= 0)) != negated));
        return isTrue(valid, message, context);
    }

}
