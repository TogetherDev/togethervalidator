package com.togetherdev.util.validator.constraintvalidators.time;

import com.togetherdev.util.validator.constraints.time.Past;
import static com.togetherdev.util.validator.util.InstanceFactory.newZoneIdProvider;
import java.time.ZoneId;
import java.time.temporal.Temporal;
import java.util.function.Function;

public abstract class AbstractPastValidatorForTemporal<T extends Temporal & Comparable<? super T>> extends AbstractTemporalValidator<Past, T> {

    public AbstractPastValidatorForTemporal() {
        super(false);
    }

    public AbstractPastValidatorForTemporal(Function<ZoneId, T> currentTimeSupplier) {
        this();
        setCurrentTimeSupplier(currentTimeSupplier);
    }

    @Override
    public void initialize(Past annotation) {
        setCurrentTimeValid(annotation.currentTimeValid());
        setDefaultZoneIdProvider(newZoneIdProvider(annotation.defaultZoneIdProvider()));
        setMinDifference(annotation.minDifference(), annotation.minDifferenceUnit());
        setMaxDifference(annotation.maxDifference(), annotation.maxDifferenceUnit());
        setMessage(annotation.message());
        setMaxLimitMessage(annotation.maxLimitMessage());
    }

}
