package com.togetherdev.util.validator.constraintvalidators.time;

import com.togetherdev.util.validator.constraints.time.Future;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class FutureValidators {

    public static class FutureValidatorForInstant extends AbstractFutureValidatorForTemporal<Instant> {

        @Override
        public void initialize(Future annotation) {
            super.initialize(annotation);
            Clock defaultClock = Clock.system(getDefaultZoneIdProvider().getUserZoneId());
            setCurrentTimeSupplier(zoneId -> Instant.now(defaultClock));
        }

    }

    public static class FutureValidatorForLocalDate extends AbstractFutureValidatorForTemporal<LocalDate> {

        public FutureValidatorForLocalDate() {
            super(LocalDate::now);
        }

    }

    public static class FutureValidatorForLocalDateTime extends AbstractFutureValidatorForTemporal<LocalDateTime> {

        public FutureValidatorForLocalDateTime() {
            super(LocalDateTime::now);
        }

    }

    public static class FutureValidatorForLocalTime extends AbstractFutureValidatorForTemporal<LocalTime> {

        public FutureValidatorForLocalTime() {
            super(LocalTime::now);
        }

    }

    public static class FutureValidatorForOffsetDateTime extends AbstractFutureValidatorForTemporal<OffsetDateTime> {

        public FutureValidatorForOffsetDateTime() {
            super(OffsetDateTime::now);
        }

        @Override
        protected ZoneId getZoneId(OffsetDateTime dateTime, ZoneId defaultZoneId) {
            return dateTime.getOffset();
        }

    }

    public static class FutureValidatorForOffsetTime extends AbstractFutureValidatorForTemporal<OffsetTime> {

        public FutureValidatorForOffsetTime() {
            super(OffsetTime::now);
        }

        @Override
        protected ZoneId getZoneId(OffsetTime time, ZoneId defaultZoneId) {
            return time.getOffset();
        }

    }

    public static class FutureValidatorForZonedDateTime extends AbstractFutureValidatorForTemporal<ZonedDateTime> {

        public FutureValidatorForZonedDateTime() {
            super(ZonedDateTime::now);
        }

        @Override
        protected ZoneId getZoneId(ZonedDateTime dateTime, ZoneId defaultZoneId) {
            return dateTime.getZone();
        }

    }

    public static class FutureValidatorForYear extends AbstractFutureValidatorForTemporal<Year> {

        public FutureValidatorForYear() {
            super(Year::now);
        }

    }

    public static class FutureValidatorForYearMonth extends AbstractFutureValidatorForTemporal<YearMonth> {

        public FutureValidatorForYearMonth() {
            super(YearMonth::now);

        }

    }

}
