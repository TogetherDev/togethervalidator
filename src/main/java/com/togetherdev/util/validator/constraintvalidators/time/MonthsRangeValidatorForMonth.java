package com.togetherdev.util.validator.constraintvalidators.time;

import com.togetherdev.util.validator.constraints.time.MonthsRange;
import com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidator;
import java.time.Month;

public class MonthsRangeValidatorForMonth extends AbstractRangeValidator<MonthsRange, Month> {

    public MonthsRangeValidatorForMonth() {
        super(Month::compareTo);
    }

    @Override
    public void initialize(MonthsRange annotation) {
        this.negated = annotation.negated();
        super.initialize(annotation.min(), annotation.max(), annotation.message());
    }

}
