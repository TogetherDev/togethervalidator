package com.togetherdev.util.validator.constraintvalidators.text;

import com.togetherdev.util.validator.constraints.text.UniqueString;
import com.togetherdev.util.validator.constraints.text.UniqueString.UniqueStringTester;
import static com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidator.isTrue;
import com.togetherdev.util.validator.util.InstanceFactory;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueStringValidatorForString implements ConstraintValidator<UniqueString, String> {

    private String message;
    private UniqueStringTester validator;

    @Override
    public void initialize(UniqueString annotation) {
        message = annotation.message();
        validator = InstanceFactory.newInstance(annotation.tester());
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        return isTrue(((value == null) || validator.isUnique(value)), message, context);
    }

}
