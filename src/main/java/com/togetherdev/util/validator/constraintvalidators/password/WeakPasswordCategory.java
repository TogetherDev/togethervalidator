package com.togetherdev.util.validator.constraintvalidators.password;

import static com.togetherdev.util.validator.TogetherValidatorConfiguration.getResourceAsLines;
import java.util.List;

public enum WeakPasswordCategory {

    ANIMALS("WeakPasswords-Animals.properties"),
    COLORS("WeakPasswords-Colors.properties"),
    FOODS("WeakPasswords-Foods.properties"),
    NOOBS("WeakPasswords-Noobs.properties"),
    NUMBERS("WeakPasswords-Numbers.properties"),
    PERSONS("WeakPasswords-Persons.properties");

    private final String FILE_NAME;

    private WeakPasswordCategory(String fileName) {
        this.FILE_NAME = fileName;
    }

    public String getFileName() {
        return FILE_NAME;
    }

    public List<String> getResource() {
        return getResourceAsLines(FILE_NAME);
    }

    public static List<String> getResource(WeakPasswordCategory... categories) {
        List<String> allLines = categories[0].getResource();
        for (int i = 1; i < categories.length; i++) {
            allLines.addAll(categories[i].getResource());
        }
        return allLines;
    }

}
