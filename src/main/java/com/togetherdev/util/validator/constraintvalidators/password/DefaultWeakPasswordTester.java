package com.togetherdev.util.validator.constraintvalidators.password;

import static com.togetherdev.util.validator.TogetherValidatorConfiguration.getResourceAsLines;
import com.togetherdev.util.validator.constraints.password.Password.WeakPasswordTester;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

public class DefaultWeakPasswordTester implements WeakPasswordTester {

    public static final String WEAK_PASSWORD_VARIANTS_FILE = "WeakPasswordVariants.properties";
    private final boolean CASE_INSENSITIVE;
    private final Map<String, String> WEAK_PASSWORDS;
    private final Set<String> WEAK_PASSWORDS_VARIANTS;

    public static DefaultWeakPasswordTester getInstance() {
        return DefaultWeakPasswordTesterHolder.INSTANCE;
    }

    public DefaultWeakPasswordTester() {
        this(WeakPasswordCategory.values());
    }

    public DefaultWeakPasswordTester(WeakPasswordCategory... categories) {
        this(true, getResourceAsLines(WEAK_PASSWORD_VARIANTS_FILE), categories);
    }

    public DefaultWeakPasswordTester(boolean caseInsensitive, List<String> weakPasswordVariants, WeakPasswordCategory... categories) {
        this(caseInsensitive, WeakPasswordCategory.getResource(categories), weakPasswordVariants);
    }

    public DefaultWeakPasswordTester(boolean caseInsensitive, List<String> weakPasswords, List<String> weakPasswordsVariants) {
        this.CASE_INSENSITIVE = caseInsensitive;
        this.WEAK_PASSWORDS = new HashMap<>(weakPasswords.size());
        weakPasswords.forEach(line -> {
            if (!line.isEmpty()) {
                if (caseInsensitive) {
                    line = line.toLowerCase();
                }
                WEAK_PASSWORDS.put(line, Pattern.quote(line));
            }
        });
        this.WEAK_PASSWORDS_VARIANTS = new HashSet<>(weakPasswordsVariants.size());
        weakPasswordsVariants.forEach(v -> {
            if (!v.isEmpty()) {
                if (caseInsensitive) {
                    v = v.toLowerCase();
                }
                WEAK_PASSWORDS_VARIANTS.add(v);
            }
        });
    }

    public boolean isCaseInsensitive() {
        return CASE_INSENSITIVE;
    }

    @Override
    public boolean isWeakPassword(String password) {
        password = password.trim();
        if (CASE_INSENSITIVE) {
            password = password.toLowerCase();
        }
        if (WEAK_PASSWORDS.containsKey(password)) {
            return true;
        }
        for (Entry<String, String> weakPassword : WEAK_PASSWORDS.entrySet()) {
            if (password.contains(weakPassword.getKey()) && isWeakPassword(password.split(weakPassword.getValue()))) {
                return true;
            }
        }
        return false;
    }

    protected boolean isWeakPassword(String[] splittedPassword) {
        if (splittedPassword.length == 0) {
            return true;
        }
        for (String text : splittedPassword) {
            if ((!text.isEmpty()) && WEAK_PASSWORDS_VARIANTS.contains(text)) {
                return true;
            }
        }
        return false;
    }

    private static class DefaultWeakPasswordTesterHolder {

        private static final DefaultWeakPasswordTester INSTANCE = new DefaultWeakPasswordTester();

    }

}
