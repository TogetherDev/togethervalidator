package com.togetherdev.util.validator.constraintvalidators.text;

import com.togetherdev.util.validator.constraints.text.StringEquals;
import com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidator;

public class StringEqualsValidatorForCharSequence extends AbstractGroupValidator<StringEquals, CharSequence, String> {

    @Override
    public void initialize(StringEquals annotation) {
        boolean caseSensitive = annotation.caseSensitive();
        boolean useTrimMethod = annotation.useTrimMethod();
        this.predicate = (x, y) -> equals(caseSensitive, useTrimMethod, x, y);
        this.negated = annotation.negated();
        super.initialize(annotation.values(), annotation.message());
    }

    public static boolean equals(boolean caseSensitive, boolean useTrimMethod, CharSequence x, String y) {
        if ((x instanceof String) || caseSensitive || useTrimMethod) {
            return equals(caseSensitive, useTrimMethod, x.toString(), y);
        }
        return x.equals(y);
    }

    public static boolean equals(boolean caseSensitive, boolean useTrimMethod, String x, String y) {
        if (useTrimMethod) {
            x = x.trim();
        }
        return (caseSensitive ? x.equals(y) : x.equalsIgnoreCase(y));
    }

}
