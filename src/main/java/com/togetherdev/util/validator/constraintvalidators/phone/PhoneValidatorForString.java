package com.togetherdev.util.validator.constraintvalidators.phone;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import static com.togetherdev.util.format.Formatters.formatWithBraces;
import static com.togetherdev.util.validator.constraintvalidators.phone.AbstractPhoneValidator.PHONE_NUMBER_UTIL;
import javax.validation.ConstraintValidatorContext;

/**
 * This class is a phone validator for {@linkplain String} that uses the <strong>libphonenumber API</strong> to validate the numbers.
 *
 * @author Thomás Sousa Silva
 */
public class PhoneValidatorForString extends AbstractPhoneValidator<String> {

    /**
     * Validates the given phone. A phone is valid if is
     * {@linkplain PhoneNumberUtil#parse(java.lang.String, java.lang.String) parsable},
     * {@linkplain PhoneNumberUtil#isValidNumber(com.google.i18n.phonenumbers.Phonenumber.PhoneNumber) a valid number},
     * {@link PhoneNumberUtil#isValidNumberForRegion(com.google.i18n.phonenumbers.Phonenumber.PhoneNumber, java.lang.String) a valid number for defined regions} and
     * {@linkplain PhoneNumberUtil#getNumberType(com.google.i18n.phonenumbers.Phonenumber.PhoneNumber) is of a defined type}.
     *
     * @param number The phone that will be checked.
     * @param context The context that will be used to add the constraint violations.
     * @return True if it is valid.
     */
    @Override
    public boolean isValid(String number, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        if (number == null) {
            return true;
        }
        PhoneNumber phone = parse(number, context);
        return isValid(phone, context);
    }

    /**
     * {@linkplain PhoneNumberUtil#parse(java.lang.String, java.lang.String) Parse} the given String and returns a PhoneNumber.
     *
     * @param number The number that will be parsed.
     * @param context The context that will be used to add the constraint violations.
     * @return A PhoneNumber that represents the given String.
     */
    private PhoneNumber parse(String number, ConstraintValidatorContext context) {
        try {
            return PHONE_NUMBER_UTIL.parse(number, annotation.defaultRegion());
        } catch (NumberParseException ex) {
            context.buildConstraintViolationWithTemplate(formatWithBraces(ex.getErrorType())).addConstraintViolation();
            return null;
        }
    }

}
