package com.togetherdev.util.validator.constraintvalidators.number;

import com.togetherdev.util.validator.constraints.number.IntegersGroup;
import com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidator;

public class IntegersGroupValidatorForNumber extends AbstractGroupValidator<IntegersGroup, Number, Number> {

    public IntegersGroupValidatorForNumber() {
        super((n1, n2) -> (n1.longValue() == n2.longValue()));
    }

    @Override
    public void initialize(IntegersGroup annotation) {
        long[] values = annotation.values();
        Number[] numbers = new Number[values.length];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = values[i];
        }
        this.negated = annotation.negated();
        super.initialize(numbers, annotation.message());
    }

}
