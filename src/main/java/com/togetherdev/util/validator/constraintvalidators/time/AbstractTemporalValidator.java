package com.togetherdev.util.validator.constraintvalidators.time;

import static com.togetherdev.util.validator.constraintvalidation.ValidationContext.incrementDynamicAttributesDuration;
import static com.togetherdev.util.validator.constraintvalidation.ValidationContext.putDynamicAttribute;
import static com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidator.isTrue;
import com.togetherdev.util.validator.util.UserZoneIdProvider;
import java.lang.annotation.Annotation;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import static java.util.Objects.requireNonNull;
import java.util.function.Function;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public abstract class AbstractTemporalValidator<A extends Annotation, T extends Temporal & Comparable<? super T>> implements ConstraintValidator<A, T> {

    private final boolean FUTURE;
    private long minDifference;
    private long maxDifference;
    private ChronoUnit minDifferenceUnit;
    private ChronoUnit maxDifferenceUnit;
    private Function<ZoneId, T> currentTimeSupplier;
    private UserZoneIdProvider defaultZoneIdProvider;
    private boolean currentTimeValid;
    private String message;
    private String maxLimitMessage;

    public AbstractTemporalValidator(boolean future) {
        this.FUTURE = future;
    }

    @Override
    public boolean isValid(T value, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        if (value == null) {
            return true;
        }
        ZoneId zoneId = getZoneId(value, defaultZoneIdProvider.getUserZoneId());
        T currentTime = currentTimeSupplier.apply(zoneId);
        return (isWithinMinLimit(value, currentTime, context) && isWithinMaxLimit(value, currentTime, context));
    }

    private boolean isWithinMinLimit(T value, T currentTime, ConstraintValidatorContext context) {
        boolean withMinDifference = (minDifference > 0);
        if (withMinDifference) {
            currentTime = getMinLimit(currentTime);
            putDynamicAttribute("minLimit", currentTime);
            incrementDynamicAttributesDuration();
        }
        return isTrue((isWithinMinLimit(value, currentTime, (currentTimeValid || withMinDifference))), message, context);
    }

    private boolean isWithinMinLimit(T value, T currentTime, boolean equalIsValid) {
        int result = value.compareTo(currentTime);
        if (equalIsValid) {
            return (FUTURE ? (result >= 0) : (result <= 0));
        }
        return (FUTURE ? (result > 0) : (result < 0));
    }

    private boolean isWithinMaxLimit(T value, T currentTime, ConstraintValidatorContext context) {
        if (maxDifference > 0) {
            T maxLimit = getMaxLimit(currentTime);
            int result = value.compareTo(maxLimit);
            if (FUTURE ? (result <= 0) : (result >= 0)) {
                return true;
            }
            context.buildConstraintViolationWithTemplate(maxLimitMessage).addConstraintViolation();
            putDynamicAttribute("maxLimit", maxLimit);
            incrementDynamicAttributesDuration();
            return false;
        }
        return true;

    }

    protected UserZoneIdProvider getDefaultZoneIdProvider() {
        return defaultZoneIdProvider;
    }

    protected ZoneId getZoneId(T value, ZoneId defaultZoneId) {
        return defaultZoneId;
    }

    private T getMinLimit(T currentTime) {
        return getAdjustedTime(currentTime, minDifference, minDifferenceUnit);
    }

    private T getMaxLimit(T currentTime) {
        return getAdjustedTime(currentTime, maxDifference, maxDifferenceUnit);
    }

    private T getAdjustedTime(T time, long difference, ChronoUnit differenceUnit) {
        if (FUTURE) {
            return (T) time.plus(difference, differenceUnit);
        }
        return (T) time.minus(difference, differenceUnit);
    }

    protected void setCurrentTimeSupplier(Function<ZoneId, T> currentTimeSupplier) {
        this.currentTimeSupplier = requireNonNull(currentTimeSupplier);
    }

    protected void setDefaultZoneIdProvider(UserZoneIdProvider defaultZoneIdProvider) {
        this.defaultZoneIdProvider = requireNonNull(defaultZoneIdProvider);
    }

    protected void setMinDifference(long minDifference, ChronoUnit minDifferenceUnit) {
        this.minDifference = minDifference;
        this.minDifferenceUnit = requireNonNull(minDifferenceUnit);
    }

    protected void setMaxDifference(long maxDifference, ChronoUnit maxDifferenceUnit) {
        this.maxDifference = maxDifference;
        this.maxDifferenceUnit = requireNonNull(maxDifferenceUnit);
    }

    protected void setCurrentTimeValid(boolean currentTimeValid) {
        this.currentTimeValid = currentTimeValid;
    }

    protected void setMessage(String message) {
        this.message = requireNonNull(message);
    }

    protected void setMaxLimitMessage(String maxLimitMessage) {
        this.maxLimitMessage = requireNonNull(maxLimitMessage);
    }

}
