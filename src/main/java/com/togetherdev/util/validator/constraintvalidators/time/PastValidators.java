package com.togetherdev.util.validator.constraintvalidators.time;

import com.togetherdev.util.validator.constraints.time.Past;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.OffsetTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class PastValidators {

    public static class PastValidatorForInstant extends AbstractPastValidatorForTemporal<Instant> {

        @Override
        public void initialize(Past annotation) {
            super.initialize(annotation);
            Clock defaultClock = Clock.system(getDefaultZoneIdProvider().getUserZoneId());
            setCurrentTimeSupplier(zoneId -> Instant.now(defaultClock));
        }

    }

    public static class PastValidatorForLocalDate extends AbstractPastValidatorForTemporal<LocalDate> {

        public PastValidatorForLocalDate() {
            super(LocalDate::now);
        }

    }

    public static class PastValidatorForLocalDateTime extends AbstractPastValidatorForTemporal<LocalDateTime> {

        public PastValidatorForLocalDateTime() {
            super(LocalDateTime::now);
        }

    }

    public static class PastValidatorForLocalTime extends AbstractPastValidatorForTemporal<LocalTime> {

        public PastValidatorForLocalTime() {
            super(LocalTime::now);
        }

    }

    public static class PastValidatorForOffsetDateTime extends AbstractPastValidatorForTemporal<OffsetDateTime> {

        public PastValidatorForOffsetDateTime() {
            super(OffsetDateTime::now);
        }

        @Override
        protected ZoneId getZoneId(OffsetDateTime dateTime, ZoneId defaultZoneId) {
            return dateTime.getOffset();
        }

    }

    public static class PastValidatorForOffsetTime extends AbstractPastValidatorForTemporal<OffsetTime> {

        public PastValidatorForOffsetTime() {
            super(OffsetTime::now);
        }

        @Override
        protected ZoneId getZoneId(OffsetTime time, ZoneId defaultZoneId) {
            return time.getOffset();
        }

    }

    public static class PastValidatorForZonedDateTime extends AbstractPastValidatorForTemporal<ZonedDateTime> {

        public PastValidatorForZonedDateTime() {
            super(ZonedDateTime::now);
        }

        @Override
        protected ZoneId getZoneId(ZonedDateTime dateTime, ZoneId defaultZoneId) {
            return dateTime.getZone();
        }

    }

    public static class PastValidatorForYear extends AbstractPastValidatorForTemporal<Year> {

        public PastValidatorForYear() {
            super(Year::now);
        }

    }

    public static class PastValidatorForYearMonth extends AbstractPastValidatorForTemporal<YearMonth> {

        public PastValidatorForYearMonth() {
            super(YearMonth::now);
        }

    }

}
