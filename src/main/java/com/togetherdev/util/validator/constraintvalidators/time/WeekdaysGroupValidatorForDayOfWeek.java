package com.togetherdev.util.validator.constraintvalidators.time;

import com.togetherdev.util.validator.constraints.time.WeekdaysGroup;
import com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidator;
import java.time.DayOfWeek;

public class WeekdaysGroupValidatorForDayOfWeek extends AbstractGroupValidator<WeekdaysGroup, DayOfWeek, DayOfWeek> {

    public WeekdaysGroupValidatorForDayOfWeek() {
        super(DayOfWeek::equals);
    }

    @Override
    public void initialize(WeekdaysGroup annotation) {
        this.negated = annotation.negated();
        super.initialize(annotation.days(), annotation.message());
    }

}
