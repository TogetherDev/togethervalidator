package com.togetherdev.util.validator.constraintvalidators.text;

import com.togetherdev.util.validator.constraints.text.StartsWith;
import com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidator;

public class StartsWithValidatorForCharSequence extends AbstractGroupValidator<StartsWith, CharSequence, String> {

    @Override
    public void initialize(StartsWith annotation) {
        boolean caseSensitive = annotation.caseSensitive();
        boolean useTrimMethod = annotation.useTrimMethod();
        this.predicate = (x, y) -> checkIfStartsWith(caseSensitive, useTrimMethod, x, y);
        this.negated = annotation.negated();
        String[] values = annotation.values();
        if (!caseSensitive) {
            for (int i = 0; i < values.length; i++) {
                values[i] = values[i].toLowerCase();
            }
        }
        super.initialize(values, annotation.message());
    }

    public static boolean checkIfStartsWith(boolean caseSensitive, boolean useTrimMethod, CharSequence x, String y) {
        if (x instanceof String) {
            return checkIfStartsWith(caseSensitive, useTrimMethod, (String) x, y);
        }
        if (x.length() < y.length()) {
            return false;
        }
        CharSequence subSequence = x.subSequence(0, y.length());
        if (useTrimMethod) {
            subSequence = subSequence.toString().trim();
        }
        return (caseSensitive ? subSequence.toString().equalsIgnoreCase(y) : subSequence.equals(y));
    }

    public static boolean checkIfStartsWith(boolean caseSensitive, boolean useTrimMethod, String x, String y) {
        if (useTrimMethod) {
            x = x.trim();
        }
        return (caseSensitive ? x : x.toLowerCase()).startsWith(y);
    }

}
