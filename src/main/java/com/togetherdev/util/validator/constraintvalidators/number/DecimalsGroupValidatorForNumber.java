package com.togetherdev.util.validator.constraintvalidators.number;

import com.togetherdev.util.validator.constraints.number.DecimalsGroup;
import com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidator;

public class DecimalsGroupValidatorForNumber extends AbstractGroupValidator<DecimalsGroup, Number, Number> {

    public DecimalsGroupValidatorForNumber() {
        super((n1, n2) -> (n1.doubleValue() == n2.doubleValue()));
    }

    @Override
    public void initialize(DecimalsGroup annotation) {
        double[] values = annotation.values();
        Number[] numbers = new Number[values.length];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = values[i];
        }
        this.negated = annotation.negated();
        super.initialize(numbers, annotation.message());
    }

}
