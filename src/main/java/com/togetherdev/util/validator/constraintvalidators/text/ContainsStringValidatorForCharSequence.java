package com.togetherdev.util.validator.constraintvalidators.text;

import com.togetherdev.util.validator.constraints.text.ContainsString;
import com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidator;

public class ContainsStringValidatorForCharSequence extends AbstractGroupValidator<ContainsString, CharSequence, String> {

    @Override
    public void initialize(ContainsString annotation) {
        boolean caseSensitive = annotation.caseSensitive();
        boolean useTrimMethod = annotation.useTrimMethod();
        this.predicate = (x, y) -> checkIfContains(caseSensitive, useTrimMethod, x, y);
        this.negated = annotation.negated();
        String[] values = annotation.values();
        if (!caseSensitive) {
            for (int i = 0; i < values.length; i++) {
                values[i] = values[i].toLowerCase();
            }
        }
        super.initialize(values, annotation.message());
    }

    public static boolean checkIfContains(boolean caseSensitive, boolean useTrimMethod, CharSequence x, String y) {
        String text = x.toString();
        if (useTrimMethod) {
            text = text.trim();
        }
        return (caseSensitive ? text : text.toLowerCase()).contains(y);
    }

}
