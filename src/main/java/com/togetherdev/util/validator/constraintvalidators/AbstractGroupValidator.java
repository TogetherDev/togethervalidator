package com.togetherdev.util.validator.constraintvalidators;

import java.lang.annotation.Annotation;
import static java.util.Objects.requireNonNull;
import java.util.function.BiPredicate;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * This class is a phone validator that uses the <strong>libphonenumber API</strong> to validate the numbers.
 *
 * @param <A> The annotation type.
 * @param <Y> The type of object that will be validated.
 * @author Thomás Sousa Silva
 */
public abstract class AbstractGroupValidator<A extends Annotation, X, Y> implements ConstraintValidator<A, X> {

    private Y[] values;
    private String message;
    protected boolean negated;
    protected BiPredicate<X, Y> predicate;

    public AbstractGroupValidator() {
    }

    public AbstractGroupValidator(BiPredicate<X, Y> predicate) {
        this.predicate = requireNonNull(predicate);
    }

    protected void initialize(Y[] values, String message) {
        this.values = requireNonNull(values);
        this.message = requireNonNull(message);
    }

    @Override
    public boolean isValid(X value, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        return isTrue(((value == null) || (someMatchToPredicate(value) != negated)), message, context);
    }

    private boolean someMatchToPredicate(X value) {
        for (Y y : values) {
            if (predicate.test(value, y)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isTrue(boolean value, String message, ConstraintValidatorContext context) {
        if (value) {
            return true;
        }
        context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
        return false;
    }

}
