package com.togetherdev.util.validator.constraintvalidators.time;

import com.togetherdev.util.validator.constraints.time.MonthsGroup;
import com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidator;
import java.time.Month;

public class MonthsGroupValidatorForMonth extends AbstractGroupValidator<MonthsGroup, Month, Month> {

    public MonthsGroupValidatorForMonth() {
        super(Month::equals);
    }

    @Override
    public void initialize(MonthsGroup annotation) {
        this.negated = annotation.negated();
        super.initialize(annotation.months(), annotation.message());
    }

}
