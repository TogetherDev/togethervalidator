package com.togetherdev.util.validator.constraintvalidators.phone;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberType;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.togetherdev.util.validator.constraints.phone.Phone;
import static com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidator.isTrue;
import static java.util.Arrays.binarySearch;
import static java.util.Arrays.sort;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * This class is a abstract phone validator that uses the <strong>libphonenumber API</strong> to validate the numbers.
 *
 * @author Thomás Sousa Silva
 */
public abstract class AbstractPhoneValidator<T> implements ConstraintValidator<Phone, T> {

    protected static final PhoneNumberUtil PHONE_NUMBER_UTIL = PhoneNumberUtil.getInstance();

    protected Phone annotation;
    private String[] regions;
    private PhoneNumberType[] types;

    @Override
    public void initialize(Phone annotation) {
        this.annotation = annotation;
        this.regions = annotation.regions();
        this.types = annotation.types();
        sort(types);
    }

    /**
     * Validates the given phone. A phone is valid if is
     * {@linkplain PhoneNumberUtil#isValidNumber(com.google.i18n.phonenumbers.Phonenumber.PhoneNumber) a valid number},
     * {@link PhoneNumberUtil#isValidNumberForRegion(com.google.i18n.phonenumbers.Phonenumber.PhoneNumber, java.lang.String) a valid number for defined regions} and
     * {@linkplain PhoneNumberUtil#getNumberType(com.google.i18n.phonenumbers.Phonenumber.PhoneNumber) is of a defined type}.
     *
     * @param phone The phone that will be checked.
     * @param context The context that will be used to add the constraint violations.
     * @return True if it is valid.
     */
    public boolean isValid(PhoneNumber phone, ConstraintValidatorContext context) {
        boolean validNumber = isValidNumber(phone, context);
        boolean validRegion = isValidRegion(phone, context);
        boolean validType = isValidType(phone, context);
        return (validNumber && validRegion && validType);
    }

    /**
     * Checks if the {@code phone} is a valid number.
     *
     * @param phone The phone that will be checked.
     * @param context The context that will be used to add the constraint violations.
     * @return True if the phone is of a valid region.
     */
    private boolean isValidNumber(PhoneNumber phone, ConstraintValidatorContext context) {
        return isTrue((phone != null) && PHONE_NUMBER_UTIL.isValidNumber(phone), annotation.message(), context);
    }

    /**
     * Checks if the {@code phone} is of a valid region.
     *
     * @param phone The phone that will be checked.
     * @param context The context that will be used to add the constraint violations.
     * @return True if the phone is of a valid region.
     */
    private boolean isValidRegion(PhoneNumber phone, ConstraintValidatorContext context) {
        if (regions.length == 0) {
            return true;
        }
        if (phone != null) {
            for (String region : regions) {
                if (PHONE_NUMBER_UTIL.isValidNumberForRegion(phone, region)) {
                    return true;
                }
            }
        }
        context.buildConstraintViolationWithTemplate(annotation.invalidRegionMessage()).addConstraintViolation();
        return false;
    }

    /**
     * Checks if the {@code phone} has a valid type.
     *
     * @param phone The phone that will be checked.
     * @param context The context that will be used to add the constraint violations.
     * @return True if the phone has a valid type.
     */
    private boolean isValidType(PhoneNumber phone, ConstraintValidatorContext context) {
        boolean valid = ((types.length == 0) || ((phone != null) && (binarySearch(types, PHONE_NUMBER_UTIL.getNumberType(phone)) >= 0)));
        return isTrue(valid, annotation.invalidTypeMessage(), context);
    }

}
