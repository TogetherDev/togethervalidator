package com.togetherdev.util.validator.constraintvalidators.time;

import com.togetherdev.util.validator.constraints.time.Future;
import static com.togetherdev.util.validator.util.InstanceFactory.newZoneIdProvider;
import java.time.ZoneId;
import java.time.temporal.Temporal;
import java.util.function.Function;

public abstract class AbstractFutureValidatorForTemporal<T extends Temporal & Comparable<? super T>> extends AbstractTemporalValidator<Future, T> {

    public AbstractFutureValidatorForTemporal() {
        super(true);
    }

    public AbstractFutureValidatorForTemporal(Function<ZoneId, T> currentTimeSupplier) {
        this();
        setCurrentTimeSupplier(currentTimeSupplier);
    }

    @Override
    public void initialize(Future annotation) {
        setCurrentTimeValid(annotation.currentTimeValid());
        setDefaultZoneIdProvider(newZoneIdProvider(annotation.defaultZoneIdProvider()));
        setMinDifference(annotation.minDifference(), annotation.minDifferenceUnit());
        setMaxDifference(annotation.maxDifference(), annotation.maxDifferenceUnit());
        setMessage(annotation.message());
        setMaxLimitMessage(annotation.maxLimitMessage());
    }

}
