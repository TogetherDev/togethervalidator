package com.togetherdev.util.validator.constraintvalidators.number;

import com.togetherdev.util.validator.constraints.number.IntegersRange;
import com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidator;

public class IntegersRangeValidatorForNumber extends AbstractRangeValidator<IntegersRange, Number> {

    public IntegersRangeValidatorForNumber() {
        super((n1, n2) -> Long.compare(n1.longValue(), n2.longValue()));
    }

    @Override
    public void initialize(IntegersRange annotation) {
        this.negated = annotation.negated();
        super.initialize(annotation.min(), annotation.max(), annotation.message());
    }

}
