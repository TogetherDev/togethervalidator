package com.togetherdev.util.validator.constraintvalidators.password;

import com.togetherdev.util.validator.constraints.password.Password;
import com.togetherdev.util.validator.constraints.password.Password.WeakPasswordTester;
import static com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidator.isTrue;
import static com.togetherdev.util.validator.util.InstanceFactory.newWeakPasswordTester;
import java.util.Arrays;
import static java.util.Arrays.binarySearch;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidatorForString implements ConstraintValidator<Password, String> {

    private int digits;
    private int alphabet;
    private int lowerCase;
    private int upperCase;
    private int specialChars;
    private int differentChars;
    private Password annotation;
    private char[] specialCharsDefinition;
    private WeakPasswordTester weakPasswordTester;

    @Override
    public void initialize(Password annotation) {
        if (annotation.minLength() < 1) {
            throw new IllegalArgumentException("The minimum length is less than one.");
        }
        initializeDifferentChars(annotation);
        initializeRequiredSpecialChars(annotation);
        initializeWeakPasswordTester(annotation);
        this.digits = annotation.digits();
        this.alphabet = annotation.alphabet();
        this.lowerCase = annotation.lowerCase();
        this.upperCase = annotation.upperCase();
        this.annotation = annotation;
        checkRequiredChars();
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        if (password == null) {
            return true;
        }
        int length = password.length(), foundDifferentChars = length;
        int foundDigits = 0, foundAlphabet = 0, foundUpperCase = 0, foundLowerCase = 0, foundSpecialChars = 0;
        for (int i = 0; i < length; i++) {
            char c = password.charAt(i);
            if ((digits > 0) && (foundDigits < digits) && Character.isDigit(c)) {
                foundDigits++;
            } else {
                if ((alphabet > 0) && (foundAlphabet < alphabet) && Character.isAlphabetic(c)) {
                    foundAlphabet++;
                }
                if ((lowerCase > 0) && (foundLowerCase < lowerCase) && Character.isLowerCase(c)) {
                    foundLowerCase++;
                }
                if ((upperCase > 0) && (foundUpperCase < upperCase) && Character.isUpperCase(c)) {
                    foundUpperCase++;
                }
            }
            if ((specialChars > 0) && (foundSpecialChars < specialChars) && binarySearch(specialCharsDefinition, c) >= 0) {
                foundSpecialChars++;
            }
            if ((differentChars > 1) && (password.lastIndexOf(c) != i)) {
                foundDifferentChars--;
            }
        }
        boolean validLength = isValidLength(length, context);
        boolean hasMinAmountOfDigits = hasMinAmountOfDigits(foundDigits, context);
        boolean hasMinAmountOfAlphabet = hasMinAmountOfAlphabet(foundAlphabet, context);
        boolean hasMinAmountOfLowerCase = hasMinAmountOfLowerCase(foundLowerCase, context);
        boolean hasMinAmountOfUpperCase = hasMinAmountOfUpperCase(foundUpperCase, context);
        boolean hasMinAmountOfSpecialChars = hasMinAmountOfSpecialChars(foundSpecialChars, context);
        boolean hasMinAmountOfDifferentChars = hasMinAmountOfDifferentChars(foundDifferentChars, context);
        return (validLength
                && hasMinAmountOfDigits
                && hasMinAmountOfAlphabet
                && hasMinAmountOfLowerCase
                && hasMinAmountOfUpperCase
                && hasMinAmountOfSpecialChars
                && hasMinAmountOfDifferentChars
                && isNotWeakPassword(password, context));
    }

    private boolean isNotWeakPassword(String password, ConstraintValidatorContext context) {
        return isTrue(((weakPasswordTester == null) || (!weakPasswordTester.isWeakPassword(password))), annotation.weakPasswordMessage(), context);
    }

    private boolean isValidLength(int length, ConstraintValidatorContext context) {
        return isTrue((length >= annotation.minLength()), annotation.minLengthMessage(), context);
    }

    private boolean hasMinAmountOfDigits(int foundDigits, ConstraintValidatorContext context) {
        return isTrue((foundDigits >= digits), annotation.digitsMessage(), context);
    }

    private boolean hasMinAmountOfAlphabet(int foundAlphabet, ConstraintValidatorContext context) {
        return isTrue((foundAlphabet >= alphabet), annotation.alphabetMessage(), context);
    }

    private boolean hasMinAmountOfLowerCase(int foundLowerCase, ConstraintValidatorContext context) {
        return isTrue((foundLowerCase >= lowerCase), annotation.lowerCaseMessage(), context);
    }

    private boolean hasMinAmountOfUpperCase(int foundUpperCase, ConstraintValidatorContext context) {
        return isTrue((foundUpperCase >= upperCase), annotation.upperCaseMessage(), context);
    }

    private boolean hasMinAmountOfSpecialChars(int foundSpecialChars, ConstraintValidatorContext context) {
        return isTrue((foundSpecialChars >= specialChars), annotation.specialCharsMessage(), context);
    }

    private boolean hasMinAmountOfDifferentChars(int foundDifferentChars, ConstraintValidatorContext context) {
        return isTrue((foundDifferentChars >= differentChars), annotation.differentCharsMessage(), context);
    }

    public void initializeDifferentChars(Password annotation) {
        if (annotation.differentChars() > annotation.minLength()) {
            throw new IllegalArgumentException(String.format("The different chars constraint (%d) is larger than minimum length (%d).", annotation.differentChars(), annotation.minLength()));
        }
        this.differentChars = annotation.differentChars();
    }

    private void initializeRequiredSpecialChars(Password annotation) {
        if (annotation.specialChars() > 0) {
            if (annotation.specialCharsDefinition().isEmpty()) {
                throw new IllegalArgumentException("The special chars list is empty.");
            }
            this.specialCharsDefinition = annotation.specialCharsDefinition().toCharArray();
            Arrays.sort(specialCharsDefinition);
        } else {
            this.specialCharsDefinition = new char[0];
        }
        this.specialChars = annotation.specialChars();
    }

    private void initializeWeakPasswordTester(Password annotation) {
        this.weakPasswordTester = (annotation.useWeakPasswordTester() ? newWeakPasswordTester(annotation.weakPasswordTester()) : null);
    }

    private void checkRequiredChars() {
        int requiredChars = ((digits > 0) ? digits : 0);
        int alphabetWithoutLetters = alphabet;
        if (lowerCase > 0) {
            requiredChars += lowerCase;
            alphabetWithoutLetters -= lowerCase;
        }
        if (upperCase > 0) {
            requiredChars += upperCase;
            alphabetWithoutLetters -= upperCase;
        }
        if (alphabetWithoutLetters > 0) {
            requiredChars -= alphabetWithoutLetters;
        }
        if (specialChars > 0) {
            requiredChars += specialChars;
        }
        if (requiredChars > annotation.minLength()) {
            throw new IllegalArgumentException(String.format("The chars constraints total (%d) is larger than the minimum length (%d).", requiredChars, annotation.minLength()));
        }
    }

}
