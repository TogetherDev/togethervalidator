package com.togetherdev.util.validator.constraintvalidators.phone;

import com.google.i18n.phonenumbers.Phonenumber;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import javax.validation.ConstraintValidatorContext;

/**
 * This class is a phone validator for {@linkplain PhoneNumber} that uses the <strong>libphonenumber API</strong> to validate the numbers.
 *
 * @author Thomás Sousa Silva
 */
public class PhoneValidatorForPhoneNumber extends AbstractPhoneValidator<PhoneNumber> {

    @Override
    public boolean isValid(Phonenumber.PhoneNumber phone, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        if (phone == null) {
            return true;
        }
        return super.isValid(phone, context);
    }

}
