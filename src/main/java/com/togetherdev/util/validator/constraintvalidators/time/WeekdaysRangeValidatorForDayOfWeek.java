package com.togetherdev.util.validator.constraintvalidators.time;

import com.togetherdev.util.validator.constraints.time.WeekdaysRange;
import com.togetherdev.util.validator.constraintvalidators.AbstractRangeValidator;
import java.time.DayOfWeek;

public class WeekdaysRangeValidatorForDayOfWeek extends AbstractRangeValidator<WeekdaysRange, DayOfWeek> {

    public WeekdaysRangeValidatorForDayOfWeek() {
        super(DayOfWeek::compareTo);
    }

    @Override
    public void initialize(WeekdaysRange annotation) {
        this.negated = annotation.negated();
        super.initialize(annotation.min(), annotation.max(), annotation.message());
    }

}
