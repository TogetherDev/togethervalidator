package com.togetherdev.util.validator.constraintvalidators.text;

import com.togetherdev.util.validator.constraints.text.EndsWith;
import com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidator;

public class EndsWithValidatorForCharSequence extends AbstractGroupValidator<EndsWith, CharSequence, String> {

    @Override
    public void initialize(EndsWith annotation) {
        boolean caseSensitive = annotation.caseSensitive();
        boolean useTrimMethod = annotation.useTrimMethod();
        this.predicate = (x, y) -> checkIfEndsWith(caseSensitive, useTrimMethod, x, y);
        this.negated = annotation.negated();
        String[] values = annotation.values();
        if (!caseSensitive) {
            for (int i = 0; i < values.length; i++) {
                values[i] = values[i].toLowerCase();
            }
        }
        super.initialize(annotation.values(), annotation.message());
    }

    public static boolean checkIfEndsWith(boolean caseSensitive, boolean useTrimMethod, CharSequence x, String y) {
        if (x instanceof String) {
            return checkIfEndsWith(caseSensitive, useTrimMethod, (String) x, y);
        }
        int xLength = x.length(), yLength = y.length();
        if (xLength < yLength) {
            return false;
        }
        CharSequence subSequence = x.subSequence((xLength - yLength - 1), xLength);
        if (useTrimMethod) {
            subSequence = subSequence.toString().trim();
        }
        return (caseSensitive ? subSequence.toString().equalsIgnoreCase(y) : subSequence.equals(y));
    }

    public static boolean checkIfEndsWith(boolean caseSensitive, boolean useTrimMethod, String x, String y) {
        if (useTrimMethod) {
            x = x.trim();
        }
        return (caseSensitive ? x : x.toLowerCase()).endsWith(y);
    }

}
