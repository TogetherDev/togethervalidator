package com.togetherdev.util.validator.constraintvalidators.text;

import com.togetherdev.util.validator.constraints.text.CharsGroup;
import com.togetherdev.util.validator.constraintvalidators.AbstractGroupValidator;
import static java.lang.Character.toLowerCase;

public class CharsGroupValidatorForCharacter extends AbstractGroupValidator<CharsGroup, Character, Character> {

    @Override
    public void initialize(CharsGroup annotation) {
        boolean caseSensitive = annotation.caseSensitive();
        this.predicate = (caseSensitive ? Character::equals : (x, y) -> (toLowerCase(x) == toLowerCase(y)));
        char[] values = annotation.values();
        Character[] chars = new Character[values.length];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = (caseSensitive ? values[i] : toLowerCase(values[i]));
        }
        this.negated = annotation.negated();
        super.initialize(chars, annotation.message());
    }

}
