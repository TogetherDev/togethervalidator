package com.togetherdev.util.validator;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public interface PropertiesConfigurer {

    public Object getProperty(String key);

    public boolean setProperty(String key, String value);

    public static void loadFileConfiguration(PropertiesConfigurer configurer) {
        loadFileConfiguration(configurer, "Configuration.properties");
    }

    public static void loadFileConfiguration(PropertiesConfigurer configurer, String resourceName) {
        InputStream in = PropertiesConfigurer.class.getResourceAsStream(resourceName);
        if (in != null) {
            try (InputStream inputStream = in) {
                Properties properties = new Properties();
                properties.load(inputStream);
                properties.forEach((key, value) -> {
                    configurer.setProperty(key.toString(), value.toString());
                });
            } catch (IOException ex) {
                throw new ExceptionInInitializerError(ex);
            }
        }
    }

}
