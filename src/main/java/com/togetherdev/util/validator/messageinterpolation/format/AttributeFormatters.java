package com.togetherdev.util.validator.messageinterpolation.format;

import static com.togetherdev.util.validator.messageinterpolation.MessageInterpolationConfiguration.getConfiguration;
import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class AttributeFormatters {

    private static final Map<Class<? extends Annotation>, Map<String, AttributeFormatter<Object, Annotation>>> FORMATTERS;
    public static final AttributeFormatter DEFAULT_FORMATTER = (value, a, l) -> {
        if (value == null) {
            return getConfiguration().getNullValueRepresentation();
        }
        return value.toString();
    };

    static {
        Map<Class<? extends Annotation>, Map<String, AttributeFormatter<Object, Annotation>>> formatters = new HashMap<>();
        FORMATTERS = Collections.unmodifiableMap(formatters);
    }

    public static Map<String, AttributeFormatter<Object, Annotation>> getFormatters(Annotation annotation) {
        return FORMATTERS.getOrDefault(annotation.annotationType(), Collections.emptyMap());
    }

}
