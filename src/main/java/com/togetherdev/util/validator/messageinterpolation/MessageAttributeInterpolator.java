package com.togetherdev.util.validator.messageinterpolation;

import com.togetherdev.util.validator.constraintvalidation.ValidationContext;
import static com.togetherdev.util.validator.messageinterpolation.MessageInterpolationConfiguration.getConfiguration;
import com.togetherdev.util.validator.messageinterpolation.format.AttributeFormatter;
import static com.togetherdev.util.validator.messageinterpolation.format.AttributeFormatters.DEFAULT_FORMATTER;
import static com.togetherdev.util.validator.messageinterpolation.format.AttributeFormatters.getFormatters;
import java.lang.annotation.Annotation;
import java.util.Locale;
import java.util.Map;
import static java.util.Objects.requireNonNull;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.validation.MessageInterpolator;
import javax.validation.MessageInterpolator.Context;
import javax.validation.Validation;

/**
 * This class delegate the intepolation to the default message interpolator and after replace the keys that is in the {@link #KEY_PATTERN TogetherValidator Key Pattern}.
 *
 * @author Thomás Sousa Silva
 */
public class MessageAttributeInterpolator implements MessageInterpolator {

    /**
     * <table style="border: 1px solid black;">
     * <caption><h1>TogetherValidator Key Regex</h1></caption>
     * <thead>
     * <tr>
     * <th colspan="2" align="center" style="border: 1px solid black;">{@value #KEY_REGEX}</th>
     * </tr>
     * <tr>
     * <th align="center" style="border: 1px solid black;">Group</th>
     * <th align="center" style="border: 1px solid black;">Description</th>
     * </tr>
     * </thead>
     * <tbody>
     * <tr>
     * <td align="center" style="border: 1px solid black;">{@linkplain #FIRST_ESCAPE_BACKSLASH_GROUP 1}</td>
     * <td style="border: 1px solid black;">First escape backslash.</td>
     * </tr>
     * <tr>
     * <td align="center" style="border: 1px solid black;">{@linkplain #KEY_GROUP 2}</td>
     * <td style="border: 1px solid black;">Key (Key with square brackets).</td>
     * </tr>
     * <tr>
     * <td align="center" style="border: 1px solid black;">{@linkplain #SUB_KEY_GROUP 3}</td>
     * <td style="border: 1px solid black;">Sub key (Key without square brackets).</td>
     * </tr>
     * <tr>
     * <td align="center" style="border: 1px solid black;">{@linkplain #LAST_ESCAPE_BACKSLASH_GROUP 4}</td>
     * <td style="border: 1px solid black;">Last escape backslash.</td>
     * </tr>
     * </tbody>
     * <footer>
     * <tr>
     * <td colspan="2" style="border: 1px solid black;"><b>Note</b>: If you want escape a key of this pattern, then use: "<b>\[KEY_NAME\]</b>".</td>
     * </tr>
     * </footer>
     * </table>
     *
     * @see #KEY_PATTERN TogetherValidator Key Pattern
     */
    public static final String KEY_REGEX = "([\\\\]?)(\\[([a-zA-Z]+\\w*)([\\\\]*)\\])";
    private static final int FIRST_ESCAPE_BACKSLASH_GROUP = 1;
    private static final int LAST_ESCAPE_BACKSLASH_GROUP = 4;
    private static final int SUB_KEY_GROUP = 3;
    private static final int KEY_GROUP = 2;

    /**
     * <h1>TogetherValidator Key Pattern</h1>
     * <p>
     * The <i>TogetherValidator Key Pattern</i> uses the {@linkplain #KEY_REGEX TogetherValidator Key Regex} to find the keys, that will be replaced by the corresponding attributes. This attributes can be attributes that is defined statically in the annotation or {@linkplain com.togetherdev.util.validator.constraintvalidation.ValidationContext dynamic attributes}.</p>
     */
    public static final Pattern KEY_PATTERN = Pattern.compile(KEY_REGEX);

    private final MessageInterpolator DEFAULT_MESSAGE_INTERPOLATOR;

    public MessageAttributeInterpolator() {
        this(Validation.byDefaultProvider().configure().getDefaultMessageInterpolator());
    }

    public MessageAttributeInterpolator(MessageInterpolator defaultMessageInterpolator) {
        this.DEFAULT_MESSAGE_INTERPOLATOR = requireNonNull(defaultMessageInterpolator);
    }

    @Override
    public String interpolate(String messageTemplate, Context context) {
        return interpolate(messageTemplate, context, getConfiguration().getDefaultLocale());
    }

    @Override
    public String interpolate(String messageTemplate, Context context, Locale locale) {
        try {
            String message = DEFAULT_MESSAGE_INTERPOLATOR.interpolate(messageTemplate, context, locale);
            return concludeInterpolation(message, context, locale);
        } finally {
            ValidationContext.interpolatedMessage();
        }
    }

    private static String concludeInterpolation(String message, Context context, Locale locale) {
        Matcher matcher = KEY_PATTERN.matcher(message);
        if (matcher.find()) {
            Annotation annotation = context.getConstraintDescriptor().getAnnotation();
            Map<String, AttributeFormatter<Object, Annotation>> formatters = getFormatters(annotation);
            Map<String, Object> attributes = context.getConstraintDescriptor().getAttributes();
            Map<String, Object> dynamicAttributes = ValidationContext.getDynamicAttributes();
            int offsetIndex = 0;
            StringBuilder builder = new StringBuilder(message);
            do {
                boolean replaceKey = true;
                if (removeEscapeBackslash(builder, offsetIndex, matcher, FIRST_ESCAPE_BACKSLASH_GROUP)) {
                    offsetIndex--;
                    replaceKey = false;
                }
                if (removeEscapeBackslash(builder, offsetIndex, matcher, LAST_ESCAPE_BACKSLASH_GROUP)) {
                    offsetIndex--;
                    replaceKey = false;
                }
                if (replaceKey) {
                    String key = matcher.group(KEY_GROUP);
                    String subKey = matcher.group(SUB_KEY_GROUP);
                    AttributeFormatter formatter = formatters.getOrDefault(subKey, DEFAULT_FORMATTER);
                    Object value = attributes.get(subKey);
                    if (value == null) {
                        value = dynamicAttributes.get(subKey);
                    }
                    int keyLength = key.length();
                    int startIndex = (offsetIndex + matcher.start(KEY_GROUP));
                    String formattedValue = formatter.format(value, annotation, locale);
                    builder.replace(startIndex, (startIndex + keyLength), formattedValue);
                    offsetIndex += (formattedValue.length() - keyLength);
                }
            } while (matcher.find());
            message = builder.toString();
        }
        return message;
    }

    private static boolean removeEscapeBackslash(StringBuilder builder, int offsetIndex, Matcher matcher, int group) {
        if (matcher.group(group).isEmpty()) {
            return false;
        }
        builder.deleteCharAt((offsetIndex + matcher.start(group)));
        return true;
    }

}
