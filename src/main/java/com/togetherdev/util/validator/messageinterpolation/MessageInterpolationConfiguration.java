package com.togetherdev.util.validator.messageinterpolation;

import com.togetherdev.util.validator.PropertiesConfigurer;
import java.util.Locale;
import static java.util.Objects.requireNonNull;

public class MessageInterpolationConfiguration implements PropertiesConfigurer {

    public static final String DEFAULT_JVM_LOCALE = "DEFAULT_JVM_LOCALE";
    public static final String PROPERTY_DEFAULT_LOCALE = "com.togetherdev.util.validator.messageinterpolation.MessageInterpolationConfiguration.defaultLocale";
    public static final String PROPERTY_NULL_VALUE_REPRESENTATION = "com.togetherdev.util.validator.messageinterpolation.MessageInterpolationConfiguration.nullValueRepresentation";
    private static final MessageInterpolationConfiguration CONFIGURATION;
    private Locale defaultLocale;
    private String nullValueRepresentation;

    static {
        CONFIGURATION = new MessageInterpolationConfiguration();
        PropertiesConfigurer.loadFileConfiguration(CONFIGURATION);
    }

    public static MessageInterpolationConfiguration getConfiguration() {
        return CONFIGURATION;
    }

    private MessageInterpolationConfiguration() {
        this.defaultLocale = Locale.getDefault();
        this.nullValueRepresentation = "";
    }

    public Locale getDefaultLocale() {
        return defaultLocale;
    }

    public String getNullValueRepresentation() {
        return nullValueRepresentation;
    }

    public synchronized MessageInterpolationConfiguration setDefaultLocale(Locale defaultLocale) {
        this.defaultLocale = requireNonNull(defaultLocale);
        return this;
    }

    public synchronized MessageInterpolationConfiguration setNullValueRepresentation(String nullValueRepresentation) {
        this.nullValueRepresentation = requireNonNull(nullValueRepresentation);
        return this;
    }

    @Override
    public Object getProperty(String key) {
        switch (key) {
            case PROPERTY_DEFAULT_LOCALE:
                return getDefaultLocale();
            case PROPERTY_NULL_VALUE_REPRESENTATION:
                return getNullValueRepresentation();
            default:
                throw new IllegalArgumentException(key);
        }
    }

    @Override
    public boolean setProperty(String key, String value) {
        switch (key) {
            case PROPERTY_DEFAULT_LOCALE:
                setDefaultLocale(value.equalsIgnoreCase(DEFAULT_JVM_LOCALE) ? Locale.getDefault() : Locale.forLanguageTag(value));
                return true;
            case PROPERTY_NULL_VALUE_REPRESENTATION:
                setNullValueRepresentation(value);
                return true;
            default:
                return false;
        }
    }

}
