package com.togetherdev.util.validator.messageinterpolation.format;

import static com.togetherdev.util.validator.messageinterpolation.MessageInterpolationConfiguration.getConfiguration;
import java.lang.annotation.Annotation;
import java.util.Locale;

public interface AttributeFormatter<T, A extends Annotation> {

    public String format(T value, A annotation, Locale locale);

    public static <T, A extends Annotation> AttributeFormatter<Object, Annotation> of(Class<T> valueClass, Class<A> annotationClass, AttributeFormatter<T, A> formatter) {
        return (value, annotation, locale) -> {
            if (value == null) {
                return getConfiguration().getNullValueRepresentation();
            }
            String formattedValue = formatter.format(valueClass.cast(value), annotationClass.cast(annotation), locale);
            return ((formattedValue == null) ? getConfiguration().getNullValueRepresentation() : formattedValue);
        };
    }

}
