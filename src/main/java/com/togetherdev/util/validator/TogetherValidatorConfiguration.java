package com.togetherdev.util.validator;

import com.togetherdev.util.validator.constraintvalidation.ValidationContextConfiguration;
import com.togetherdev.util.validator.messageinterpolation.MessageInterpolationConfiguration;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Predicate;
import static java.util.stream.Collectors.toList;

public class TogetherValidatorConfiguration {

    private static final TogetherValidatorConfiguration INSTANCE = new TogetherValidatorConfiguration();

    public static TogetherValidatorConfiguration getInstance() {
        return INSTANCE;
    }

    public ValidationContextConfiguration getValidationContextConfiguration() {
        return ValidationContextConfiguration.getConfiguration();
    }

    public MessageInterpolationConfiguration getMessageInterpolationConfiguration() {
        return MessageInterpolationConfiguration.getConfiguration();
    }

    public Object getProperty(String key) {
        if (key.startsWith(MessageInterpolationConfiguration.class.getName())) {
            MessageInterpolationConfiguration.getConfiguration().getProperty(key);
        } else if (key.startsWith(ValidationContextConfiguration.class.getName())) {
            ValidationContextConfiguration.getConfiguration().getProperty(key);
        } else {
            throw new IllegalArgumentException(key);
        }
        return this;
    }

    public TogetherValidatorConfiguration setProperty(String key, String value) {
        if (key.startsWith(MessageInterpolationConfiguration.class.getName())) {
            MessageInterpolationConfiguration.getConfiguration().setProperty(key, value);
        } else if (key.startsWith(ValidationContextConfiguration.class.getName())) {
            ValidationContextConfiguration.getConfiguration().setProperty(key, value);
        } else {
            throw new IllegalArgumentException(key);
        }
        return this;
    }

    public static URL getResource(String name) {
        return TogetherValidatorConfiguration.class.getResource(name);
    }

    public static InputStream getResourceAsStream(String name) {
        return TogetherValidatorConfiguration.class.getResourceAsStream(name);
    }

    public static URI getResourceAsURI(String name) {
        try {
            return getResource(name).toURI();
        } catch (URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static List<String> getResourceAsLines(String name) {
        return readAllLines(Paths.get(getResourceAsURI(name)));
    }

    public static List<String> readAllLines(Path path) {
        return readAllLines(path, line -> !line.isEmpty());
    }

    public static List<String> readAllLines(Path path, Predicate<String> filter) {
        try {
            List<String> lines = Files.readAllLines(path);
            if (filter != null) {
                return lines.stream().filter(filter).collect(toList());
            }
            return lines;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

}
