package com.togetherdev.util.validator.constraintvalidation;

import com.togetherdev.util.validator.PropertiesConfigurer;

public class ValidationContextConfiguration implements PropertiesConfigurer {

    public static final String PROPERTY_INITIAL_MAP_SIZE = "com.togetherdev.util.validator.constraintvalidation.ValidationContextConfiguration.initialMapSize";
    public static final String PROPERTY_CLEAN_AND_REUSE = "com.togetherdev.util.validator.constraintvalidation.ValidationContextConfiguration.cleanAndReuseMap";
    private static final ValidationContextConfiguration CONFIGURATION;
    private int initialMapSize;
    private boolean cleanAndReuseMap;

    static {
        CONFIGURATION = new ValidationContextConfiguration();
        PropertiesConfigurer.loadFileConfiguration(CONFIGURATION);
    }

    public static ValidationContextConfiguration getConfiguration() {
        return CONFIGURATION;
    }

    private ValidationContextConfiguration() {
        this.initialMapSize = 5;
        this.cleanAndReuseMap = true;
    }

    public int getInitialMapSize() {
        return initialMapSize;
    }

    public boolean isCleanAndReuseMap() {
        return cleanAndReuseMap;
    }

    public synchronized ValidationContextConfiguration setInitialMapSize(int initialMapSize) {
        if (initialMapSize < 1) {
            throw new IllegalArgumentException("The initial map size is less than one.");
        }
        this.initialMapSize = initialMapSize;
        return this;
    }

    public synchronized ValidationContextConfiguration setCleanAndReuseMap(boolean cleanAndReuseMap) {
        this.cleanAndReuseMap = cleanAndReuseMap;
        return this;
    }

    @Override
    public Object getProperty(String key) {
        switch (key) {
            case PROPERTY_INITIAL_MAP_SIZE:
                return getInitialMapSize();
            case PROPERTY_CLEAN_AND_REUSE:
                return isCleanAndReuseMap();
            default:
                throw new IllegalArgumentException(key);
        }
    }

    @Override
    public boolean setProperty(String key, String value) {
        switch (key) {
            case PROPERTY_INITIAL_MAP_SIZE:
                setInitialMapSize(Integer.parseInt(value));
                return true;
            case PROPERTY_CLEAN_AND_REUSE:
                setCleanAndReuseMap(Boolean.parseBoolean(value));
                return true;
            default:
                return false;
        }
    }

}
