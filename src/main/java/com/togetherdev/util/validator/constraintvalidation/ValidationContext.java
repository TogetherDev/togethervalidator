package com.togetherdev.util.validator.constraintvalidation;

import static com.togetherdev.util.validator.constraintvalidation.ValidationContextConfiguration.getConfiguration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * <b>Dynamic attributes</b> are attributes that is generated dynamically by the validator when it is validating a value.</p>
 *
 * @author Thomás Sousa Silva
 */
public class ValidationContext {

    private static final int NULL_DURATION = -1;
    private static final ThreadLocal<Integer> DYNAMIC_ATTRIBUTES_DURATION = new ThreadLocal<>();
    private static final ThreadLocal<Map<String, Object>> DYNAMIC_ATTRIBUTES_OF_CONSTRAINT_VIOLATION = new ThreadLocal<>();

    public static Map<String, Object> getDynamicAttributes() {
        Map<String, Object> map = DYNAMIC_ATTRIBUTES_OF_CONSTRAINT_VIOLATION.get();
        return ((map == null) ? Collections.emptyMap() : map);
    }

    private static int getAttributesDuration() {
        Integer duration = DYNAMIC_ATTRIBUTES_DURATION.get();
        return ((duration == null) ? NULL_DURATION : duration);
    }

    public static void setDynamicAttributesDuration(int duration) {
        if (duration < 1) {
            throw new IllegalArgumentException("The duration is less than one.");
        }
        DYNAMIC_ATTRIBUTES_DURATION.set(duration);
    }

    public static void putDynamicAttribute(String key, Object value) {
        Map<String, Object> map = DYNAMIC_ATTRIBUTES_OF_CONSTRAINT_VIOLATION.get();
        if (map == null) {
            map = new HashMap<>(getConfiguration().getInitialMapSize());
            DYNAMIC_ATTRIBUTES_OF_CONSTRAINT_VIOLATION.set(map);
        }
        map.put(key, value);
    }

    public static void incrementDynamicAttributesDuration() {
        int attributesDuration = getAttributesDuration();
        if (attributesDuration == NULL_DURATION) {
            attributesDuration = 0;
        }
        DYNAMIC_ATTRIBUTES_DURATION.set(attributesDuration + 1);
    }

    public static void interpolatedMessage() {
        int duration = getAttributesDuration();
        if (duration == 1) {
            DYNAMIC_ATTRIBUTES_DURATION.set(NULL_DURATION);
            removeDynamicAttributes();
        } else if (duration > 1) {
            DYNAMIC_ATTRIBUTES_DURATION.set(--duration);
        }
    }

    private static void removeDynamicAttributes() {
        if (getConfiguration().isCleanAndReuseMap()) {
            Map<String, Object> map = DYNAMIC_ATTRIBUTES_OF_CONSTRAINT_VIOLATION.get();
            if (map != null) {
                map.clear();
            }
        } else {
            DYNAMIC_ATTRIBUTES_OF_CONSTRAINT_VIOLATION.remove();
        }
    }

}
